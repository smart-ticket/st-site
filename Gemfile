source 'https://rubygems.org'

# Add Ruby version
ruby '2.2.0'
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.0'
# Use PostgreSQL as the database for Active Record
gem 'pg'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
#gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# jQuery Turbolinks
gem 'jquery-turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

gem "active_model_serializers", github: "rails-api/active_model_serializers"

gem 'rails-i18n'
gem 'i18n-country-translations'

gem 'jquery-slick-rails'
gem 'classy_enum'
gem 'redcarpet'
gem 'date_validator'
gem 'bower'
gem 'compass-rails', github: 'Compass/compass-rails', branch: '2-0-stable'
gem 'foundation-rails'
gem 'font-awesome-rails'
gem 'simple_form'
gem 'nested_form', git: 'git://github.com/ryanb/nested_form.git'
gem 'carrierwave'
gem 'paperclip'
gem 'mini_magick'
gem 'auto_html'
gem 'underscore-rails'
gem 'gmaps4rails'
gem 'acts-as-taggable-on'
gem 'rails_12factor', group: :production
gem 'paper_trail'
gem 'therubyracer'
gem 'devise'
gem 'omniauth'
gem 'omniauth-facebook'
gem 'omniauth-google-oauth2'
gem 'pundit'
gem 'paranoia'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end
