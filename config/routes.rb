Rails.application.routes.draw do

  root "events#index"

  resources :countries
  resources :venues
  resources :categories
  resources :artist_types

  devise_for :users, controllers: { sessions: 'sessions', registrations: 'registrations', passwords: 'passwords', confirmations: 'confirmations', omniauth_callbacks: 'omniauth_callbacks' }

  devise_scope :user do
    delete "remove_user_avatar", to: "registrations#remove_avatar", as: :remove_user_avatar
    post "create_news_item", to: "registrations#create_news_item", as: :create_news_item
    patch "update_news_item", to: "registrations#update_news_item", as: :update_news_item
    delete "destroy_news_item/:id", to: "registrations#destroy_news_item", as: :destroy_news_item
  end

  get '/auth/:provider/callback', to: 'sessions#create'

  get "events_category/:category", to: "events#events_category", as: :events_category
  get "events_filter", to: "events#index", as: :events_filter
  get "events_manager_filter", to: "events_manager#index", as: :events_manager_filter

  get "list_categories", to: "categories#index", defaults: { format: "json" }
  get "list_categories/:parent_category", to: "categories#index", defaults: { format: "json" }

  get "statistics", to: "stats#index"

  resources :events do
    resources :gallery_images
  end

  resources :events_manager, controller: "events_manager" do
    resources :gallery_images
    post "update_featured", to: "events_manager#update_featured", as: :update_featured
    post "update_published", to: "events_manager#update_published", as: :update_published
    post "update_approval_message", to: "events_manager#update_approval_message", as: :update_approval_message
  end

  resources :users_manager, controller: "users_manager" do
    post "update_accept_message", to: "users_manager#update_accept_message", as: :update_accept_message
  end

  resources :admin, controller: "admin"

  resources :social, controller: "social", only: [:index, :show] do
    patch "comment_news_item", to: "social#comment_news_item", as: :comment_news_item
    patch "comment_event", to: "social#comment_event", as: :comment_event
    patch "update_comment", to: "social#update_comment", as: :update_comment
  end

  resources :likenewsitemships
  resources :friendships
  resources :fanships

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
