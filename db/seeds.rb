# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


####################
# CREATE COUNTRIES
####################

Country.create!(name: "AC")
Country.create!(name: "AD")
Country.create!(name: "AE")
Country.create!(name: "AF")
Country.create!(name: "AG")
Country.create!(name: "AI")
Country.create!(name: "AL")
Country.create!(name: "AM")
Country.create!(name: "AN")
Country.create!(name: "AO")
Country.create!(name: "AQ")
Country.create!(name: "AR")
Country.create!(name: "AS")
Country.create!(name: "AT")
Country.create!(name: "AU")
Country.create!(name: "AW")
Country.create!(name: "AX")
Country.create!(name: "AZ")
Country.create!(name: "BA")
Country.create!(name: "BB")
Country.create!(name: "BD")
Country.create!(name: "BE")
Country.create!(name: "BF")
Country.create!(name: "BG")
Country.create!(name: "BH")
Country.create!(name: "BI")
Country.create!(name: "BJ")
Country.create!(name: "BL")
Country.create!(name: "BM")
Country.create!(name: "BN")
Country.create!(name: "BO")
Country.create!(name: "BQ")
Country.create!(name: "BR")
Country.create!(name: "BS")
Country.create!(name: "BT")
Country.create!(name: "BV")
Country.create!(name: "BW")
Country.create!(name: "BY")
Country.create!(name: "BZ")
Country.create!(name: "CA")
Country.create!(name: "CC")
Country.create!(name: "CD")
Country.create!(name: "CF")
Country.create!(name: "CG")
Country.create!(name: "CH")
Country.create!(name: "CI")
Country.create!(name: "CK")
Country.create!(name: "CL")
Country.create!(name: "CM")
Country.create!(name: "CN")
Country.create!(name: "CO")
Country.create!(name: "CP")
Country.create!(name: "CR")
Country.create!(name: "CU")
Country.create!(name: "CV")
Country.create!(name: "CW")
Country.create!(name: "CX")
Country.create!(name: "CY")
Country.create!(name: "CZ")
Country.create!(name: "DE")
Country.create!(name: "DG")
Country.create!(name: "DJ")
Country.create!(name: "DK")
Country.create!(name: "DM")
Country.create!(name: "DO")
Country.create!(name: "DZ")
Country.create!(name: "EA")
Country.create!(name: "EC")
Country.create!(name: "EE")
Country.create!(name: "EG")
Country.create!(name: "EH")
Country.create!(name: "ER")
Country.create!(name: "ES")
Country.create!(name: "ET")
Country.create!(name: "EU")
Country.create!(name: "FI")
Country.create!(name: "FJ")
Country.create!(name: "FK")
Country.create!(name: "FM")
Country.create!(name: "FO")
Country.create!(name: "FR")
Country.create!(name: "GA")
Country.create!(name: "GB")
Country.create!(name: "GD")
Country.create!(name: "GE")
Country.create!(name: "GF")
Country.create!(name: "GG")
Country.create!(name: "GH")
Country.create!(name: "GI")
Country.create!(name: "GL")
Country.create!(name: "GM")
Country.create!(name: "GN")
Country.create!(name: "GP")
Country.create!(name: "GQ")
Country.create!(name: "GR")
Country.create!(name: "GS")
Country.create!(name: "GT")
Country.create!(name: "GU")
Country.create!(name: "GW")
Country.create!(name: "GY")
Country.create!(name: "HK")
Country.create!(name: "HM")
Country.create!(name: "HN")
Country.create!(name: "HR")
Country.create!(name: "HT")
Country.create!(name: "HU")
Country.create!(name: "IC")
Country.create!(name: "ID")
Country.create!(name: "IE")
Country.create!(name: "IL")
Country.create!(name: "IM")
Country.create!(name: "IN")
Country.create!(name: "IO")
Country.create!(name: "IQ")
Country.create!(name: "IR")
Country.create!(name: "IS")
Country.create!(name: "IT")
Country.create!(name: "JE")
Country.create!(name: "JM")
Country.create!(name: "JO")
Country.create!(name: "JP")
Country.create!(name: "KE")
Country.create!(name: "KG")
Country.create!(name: "KH")
Country.create!(name: "KI")
Country.create!(name: "KM")
Country.create!(name: "KN")
Country.create!(name: "KP")
Country.create!(name: "KR")
Country.create!(name: "KW")
Country.create!(name: "KY")
Country.create!(name: "KZ")
Country.create!(name: "LA")
Country.create!(name: "LB")
Country.create!(name: "LC")
Country.create!(name: "LI")
Country.create!(name: "LK")
Country.create!(name: "LR")
Country.create!(name: "LS")
Country.create!(name: "LT")
Country.create!(name: "LU")
Country.create!(name: "LV")
Country.create!(name: "LY")
Country.create!(name: "MA")
Country.create!(name: "MC")
Country.create!(name: "MD")
Country.create!(name: "ME")
Country.create!(name: "MF")
Country.create!(name: "MG")
Country.create!(name: "MH")
Country.create!(name: "MK")
Country.create!(name: "ML")
Country.create!(name: "MM")
Country.create!(name: "MN")
Country.create!(name: "MO")
Country.create!(name: "MP")
Country.create!(name: "MQ")
Country.create!(name: "MR")
Country.create!(name: "MS")
Country.create!(name: "MT")
Country.create!(name: "MU")
Country.create!(name: "MV")
Country.create!(name: "MW")
Country.create!(name: "MX")
Country.create!(name: "MY")
Country.create!(name: "MZ")
Country.create!(name: "NA")
Country.create!(name: "NC")
Country.create!(name: "NE")
Country.create!(name: "NF")
Country.create!(name: "NG")
Country.create!(name: "NI")
Country.create!(name: "NL")
Country.create!(name: "NO")
Country.create!(name: "NP")
Country.create!(name: "NR")
Country.create!(name: "NU")
Country.create!(name: "NZ")
Country.create!(name: "OM")
Country.create!(name: "PA")
Country.create!(name: "PE")
Country.create!(name: "PF")
Country.create!(name: "PG")
Country.create!(name: "PH")
Country.create!(name: "PK")
Country.create!(name: "PL")
Country.create!(name: "PM")
Country.create!(name: "PN")
Country.create!(name: "PR")
Country.create!(name: "PS")
Country.create!(name: "PT")
Country.create!(name: "PW")
Country.create!(name: "PY")
Country.create!(name: "QA")
Country.create!(name: "QO")
Country.create!(name: "RE")
Country.create!(name: "RO")
Country.create!(name: "RS")
Country.create!(name: "RU")
Country.create!(name: "RW")
Country.create!(name: "SA")
Country.create!(name: "SB")
Country.create!(name: "SC")
Country.create!(name: "SD")
Country.create!(name: "SE")
Country.create!(name: "SG")
Country.create!(name: "SH")
Country.create!(name: "SI")
Country.create!(name: "SJ")
country_sk = Country.create!(name: "SK")
Country.create!(name: "SL")
Country.create!(name: "SM")
Country.create!(name: "SN")
Country.create!(name: "SO")
Country.create!(name: "SR")
Country.create!(name: "SS")
Country.create!(name: "ST")
Country.create!(name: "SV")
Country.create!(name: "SX")
Country.create!(name: "SY")
Country.create!(name: "SZ")
Country.create!(name: "TA")
Country.create!(name: "TC")
Country.create!(name: "TD")
Country.create!(name: "TF")
Country.create!(name: "TG")
Country.create!(name: "TH")
Country.create!(name: "TJ")
Country.create!(name: "TK")
Country.create!(name: "TL")
Country.create!(name: "TM")
Country.create!(name: "TN")
Country.create!(name: "TO")
Country.create!(name: "TR")
Country.create!(name: "TT")
Country.create!(name: "TV")
Country.create!(name: "TW")
Country.create!(name: "TZ")
Country.create!(name: "UA")
Country.create!(name: "UG")
Country.create!(name: "UM")
country_us = Country.create!(name: "US")
Country.create!(name: "UY")
Country.create!(name: "UZ")
Country.create!(name: "VA")
Country.create!(name: "VC")
Country.create!(name: "VE")
Country.create!(name: "VG")
Country.create!(name: "VI")
Country.create!(name: "VN")
Country.create!(name: "VU")
Country.create!(name: "WF")
Country.create!(name: "WS")
Country.create!(name: "XK")
Country.create!(name: "YE")
Country.create!(name: "YT")
Country.create!(name: "ZA")
Country.create!(name: "ZM")
Country.create!(name: "ZW")
Country.create!(name: "ZZ")


####################
# CREATE CATEGORIES
####################
unless Category.exists?(name: "music")
  cat = Category.create!(name: "music")

  unless Category.exists?(name: "jazz")
    subcat = Category.create!(name: "jazz", parent: cat)

    unless Category.exists?(name: "swing")
      Category.create!(name: "swing", parent: subcat)
    end

    unless Category.exists?(name: "straight_ahead")
      Category.create!(name: "straight_ahead", parent: subcat)
    end

    unless Category.exists?(name: "bebop")
      Category.create!(name: "bebop", parent: subcat)
    end

    unless Category.exists?(name: "cool_jazz")
      Category.create!(name: "cool_jazz", parent: subcat)
    end

    unless Category.exists?(name: "crossover_jazz")
      Category.create!(name: "crossover_jazz", parent: subcat)
    end

    unless Category.exists?(name: "fusion")
      Category.create!(name: "fusion", parent: subcat)
    end

    unless Category.exists?(name: "soul_jazz")
      Category.create!(name: "soul_jazz", parent: subcat)
    end

    unless Category.exists?(name: "smooth_jazz")
      Category.create!(name: "smooth_jazz", parent: subcat)
    end
  end

  unless Category.exists?(name: "rock")
    subcat = Category.create!(name: "rock", parent: cat)

    unless Category.exists?(name: "punk_rock")
      Category.create!(name: "punk_rock", parent: subcat)
    end

    unless Category.exists?(name: "indie_rock")
      Category.create!(name: "indie_rock", parent: subcat)
    end
  end

  unless Category.exists?(name: "classical_music")
    subcat = Category.create!(name: "classical_music", parent: cat)

    unless Category.exists?(name: "opera")
      Category.create!(name: "opera", parent: subcat)
    end

    unless Category.exists?(name: "chamber_music")
      Category.create!(name: "chamber_music", parent: subcat)
    end

    unless Category.exists?(name: "early_music")
      Category.create!(name: "early_music", parent: subcat)
    end
  end

  unless Category.exists?(name: "electronic_music")
    subcat = Category.create!(name: "electronic_music", parent: cat)

    unless Category.exists?(name: "edm")
      Category.create!(name: "edm", parent: subcat)
    end

    unless Category.exists?(name: "techno")
      Category.create!(name: "techno", parent: subcat)
    end

    unless Category.exists?(name: "house")
      Category.create!(name: "house", parent: subcat)
    end

    unless Category.exists?(name: "noise")
      Category.create!(name: "noise", parent: subcat)
    end
  end

  unless Category.exists?(name: "pop")
    subcat = Category.create!(name: "pop", parent: cat)
  end
end

unless Category.exists?(name: "theater")
  cat = Category.create!(name: "theater")

  unless Category.exists?(name: "musical")
    subcat = Category.create!(name: "musical", parent: cat)
  end

  unless Category.exists?(name: "comedy")
    subcat = Category.create!(name: "comedy", parent: cat)
  end

  unless Category.exists?(name: "drama")
    subcat = Category.create!(name: "drama", parent: cat)
  end
end

unless Category.exists?(name: "sport")
  cat = Category.create!(name: "sport")

  unless Category.exists?(name: "fitness")
    subcat = Category.create!(name: "fitness", parent: cat)
  end

  unless Category.exists?(name: "martial_arts")
    subcat = Category.create!(name: "martial_arts", parent: cat)
  end

  unless Category.exists?(name: "hockey")
    subcat = Category.create!(name: "hockey", parent: cat)
  end

  unless Category.exists?(name: "soccer")
    subcat = Category.create!(name: "soccer", parent: cat)
  end

  unless Category.exists?(name: "tennis")
    subcat = Category.create!(name: "tennis", parent: cat)
  end

  unless Category.exists?(name: "auto_moto")
    subcat = Category.create!(name: "auto_moto", parent: cat)
  end

  unless Category.exists?(name: "athletics")
    subcat = Category.create!(name: "athletics", parent: cat)
  end
end

unless Category.exists?(name: "seminars_workshops")
  Category.create!(name: "seminars_workshops")
end

unless Category.exists?(name: "other")
  Category.create!(name: "other")
end

####################
# CREATE VENUES
####################
unless Venue.exists?(name: "Design Factory")
  Venue.create!(name: "Design Factory", street: "Bottova 2", zip: 81109, city: "Bratislava", lat: 48.143918, lng: 17.127404)
end

unless Venue.exists?(name: "Ateliér Babylon")
  Venue.create!(name: "Ateliér Babylon", street: "Námestie SNP 14", zip: 81106, city: "Bratislava", lat: 48.145415, lng: 17.112605)
end

unless Venue.exists?(name: "Slovenský rozhlas")
  ven = Venue.create!(name: "Slovenský rozhlas", street: "Mýtna", zip: 81107, city: "Bratislava", lat: 48.153938, lng: 17.114355)

  unless Venue.exists?(name: "Štúdio 3")
    subven = Venue.create!(name: "Štúdio 3", parent: ven)
  end

  unless Venue.exists?(name: "Štúdio 5")
    subven = Venue.create!(name: "Štúdio 5", parent: ven)
  end
end

unless Venue.exists?(name: "Refinery Gallery")
  Venue.create!(name: "Refinery Gallery", street: "Vlčie Hrdlo 1", zip: 82107, city: "Bratislava", lat: 48.127771, lng: 17.180148)
end

unless Venue.exists?(name: "Reduta")
  Venue.create!(name: "Reduta", street: "Medená 3", zip: 81102, city: "Bratislava", lat: 48.140577, lng: 17.110028)
end

unless Venue.exists?(name: "Národné tenisové centrum")
  Venue.create!(name: "Národné tenisové centrum", street: "Odbojárov", zip: 83103, city: "Bratislava", lat: 48.163221, lng: 17.134492)
end

unless Venue.exists?(name: "Incheba")
  ven = Venue.create!(name: "Incheba", street: "Viedenská cesta", zip: 83103, city: "Bratislava", lat: 48.163221, lng: 17.134492)

  unless Venue.exists?(name: "Expo Aréna")
    subven = Venue.create!(name: "Expo Aréna", parent: ven)
  end
end

unless Venue.exists?(name: "Randal Club")
  Venue.create!(name: "Randal Club", street: "Karpatská 2", zip: 81105, city: "Bratislava", lat: 48.156911, lng: 17.113476)
end

unless Venue.exists?(name: "Stará tržnica")
  Venue.create!(name: "Stará tržnica", street: "Námestie SNP 25", zip: 81101, city: "Bratislava", lat: 48.1445678, lng: 17.1116631)
end

unless Venue.exists?(name: "Dóm sv. Martina")
  Venue.create!(name: "Dóm sv. Martina", street: "Rudnayovo námestie 1", zip: 81101, city: "Bratislava", lat: 48.142001, lng: 17.105015)
end

unless Venue.exists?(name: "AEGON aréna")
  Venue.create!(name: "AEGON aréna", street: "Príkopova 6", zip: 83103, city: "Bratislava", lat: 48.1631592, lng: 17.134151)
end

unless Venue.exists?(name: "Letisko Trenčín")
  ven = Venue.create!(name: "Letisko Trenčín", city: "Trenčín", lat: 48.8652803, lng: 17.9996556)

  unless Venue.exists?(name: "Red Bull Tour Bus")
    Venue.create!(name: "Red Bull Tour Bus", parent: ven)
  end

  unless Venue.exists?(name: "Red Bull Tour Bus")
    Venue.create!(name: "Red Bull Tour Bus", parent: ven)
  end

  unless Venue.exists?(name: "Red Bull Tour Bus")
    Venue.create!(name: "Red Bull Tour Bus", parent: ven)
  end

  unless Venue.exists?(name: "Bažant Stage")
    Venue.create!(name: "Bažant Stage", parent: ven)
  end

  unless Venue.exists?(name: "Orange Stage")
    Venue.create!(name: "Orange Stage", parent: ven)
  end

  unless Venue.exists?(name: "Space Aréna SLSP")
    Venue.create!(name: "Space Aréna SLSP", parent: ven)
  end

  unless Venue.exists?(name: "Európa Stage")
    Venue.create!(name: "Európa Stage", parent: ven)
  end
end


####################
# CREATE ARTIST TYPES
####################
unless ArtistType.exists?(name: "band")
  artist_type_band = ArtistType.create!(name: "band")
end

unless ArtistType.exists?(name: "musician")
  artist_type_musician = ArtistType.create!(name: "musician")
end

unless ArtistType.exists?(name: "athlete")
  artist_type_athlete = ArtistType.create!(name: "athlete")
end

unless ArtistType.exists?(name: "actor")
  artist_type_actor = ArtistType.create!(name: "actor")
end


####################
# CREATE USERS
####################

unless User.exists?(email: "superadmin@smartticket.sk")
  admin = User.new(
    email: 'superadmin@smartticket.sk',
    password: 'password',
    password_confirmation: 'password',
    persistent: true,
    role: :admin
  )
  admin.skip_confirmation!
  admin.save!
end

unless User.exists?(email: "imperium@smartticket.sk")
  organizer1 = User.new(
    email: 'imperium@smartticket.sk',
    nickname: 'imperium',
    password: 'password',
    password_confirmation: 'password',
    name: 'Galaktické impérium',
    short_description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras aliquet volutpat dapibus. Sed at ante accumsan, semper arcu a, vulputate lorem. Integer condimentum luctus vulputate.',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras aliquet volutpat dapibus. Sed at ante accumsan, semper arcu a, vulputate lorem. Integer condimentum luctus vulputate. Pellentesque porttitor mattis interdum. Suspendisse finibus ullamcorper lorem vel fringilla. Morbi diam diam, eleifend ut erat vel, sodales aliquet leo. Nullam pellentesque, tortor hendrerit semper interdum, purus mauris pellentesque lacus, quis sodales nibh orci et dui. Donec molestie eu dolor efficitur dictum. Sed ac purus dolor. Morbi a erat id purus congue facilisis in ac diam. Etiam in gravida magna. Quisque fermentum elit interdum dolor suscipit feugiat. Nunc auctor nunc eu ante bibendum, pulvinar cursus metus tincidunt. Quisque vel lorem lobortis, volutpat turpis sit amet, feugiat neque.',
    avatar: File.open("#{Rails.root}/img/empire.png"),
    web: "http://www.imperium.ge",
    address: "Swooth 10",
    zip: "20922",
    city: "Ionba",
    country: country_sk,
    accepted: true,
    role: :organizer
  )
  organizer1.create_accept_message(content: "Váš profil bol schválený.")
  organizer1.skip_confirmation!
  organizer1.save!
end

unless User.exists?(email: "rebeli@smartticket.sk")
  organizer2 = User.new(
    email: 'rebeli@smartticket.sk',
    nickname: 'rebeli',
    password: 'password',
    password_confirmation: 'password',
    name: 'Rebeli',
    short_description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras aliquet volutpat dapibus. Sed at ante accumsan, semper arcu a, vulputate lorem. Integer condimentum luctus vulputate.',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras aliquet volutpat dapibus. Sed at ante accumsan, semper arcu a, vulputate lorem. Integer condimentum luctus vulputate. Pellentesque porttitor mattis interdum. Suspendisse finibus ullamcorper lorem vel fringilla. Morbi diam diam, eleifend ut erat vel, sodales aliquet leo. Nullam pellentesque, tortor hendrerit semper interdum, purus mauris pellentesque lacus, quis sodales nibh orci et dui. Donec molestie eu dolor efficitur dictum. Sed ac purus dolor. Morbi a erat id purus congue facilisis in ac diam. Etiam in gravida magna. Quisque fermentum elit interdum dolor suscipit feugiat. Nunc auctor nunc eu ante bibendum, pulvinar cursus metus tincidunt. Quisque vel lorem lobortis, volutpat turpis sit amet, feugiat neque.',
    avatar: File.open("#{Rails.root}/img/rebels.png"),
    web: "http://www.rebeli.rb",
    address: "Naboo 90",
    zip: "52108",
    city: "Kwankga",
    country: country_sk,
    accepted: true,
    role: :organizer
  )
  organizer2.create_accept_message(content: "Váš profil bol schválený.")
  organizer2.skip_confirmation!
  organizer2.save!
end

unless User.exists?(email: "federacia@smartticket.sk")
  organizer3 = User.new(
    email: 'federacia@smartticket.sk',
    nickname: 'federacia',
    password: 'password',
    password_confirmation: 'password',
    name: 'Obchodná federácia',
    short_description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras aliquet volutpat dapibus. Sed at ante accumsan, semper arcu a, vulputate lorem. Integer condimentum luctus vulputate.',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras aliquet volutpat dapibus. Sed at ante accumsan, semper arcu a, vulputate lorem. Integer condimentum luctus vulputate. Pellentesque porttitor mattis interdum. Suspendisse finibus ullamcorper lorem vel fringilla. Morbi diam diam, eleifend ut erat vel, sodales aliquet leo. Nullam pellentesque, tortor hendrerit semper interdum, purus mauris pellentesque lacus, quis sodales nibh orci et dui. Donec molestie eu dolor efficitur dictum. Sed ac purus dolor. Morbi a erat id purus congue facilisis in ac diam. Etiam in gravida magna. Quisque fermentum elit interdum dolor suscipit feugiat. Nunc auctor nunc eu ante bibendum, pulvinar cursus metus tincidunt. Quisque vel lorem lobortis, volutpat turpis sit amet, feugiat neque.',
    avatar: File.open("#{Rails.root}/img/trade.png"),
    web: "http://www.federacia.fac",
    address: "Bewtorh 10",
    zip: "88101",
    city: "Unitra",
    country: country_sk,
    accepted: true,
    role: :organizer
  )
  organizer3.create_accept_message(content: "Váš profil bol schválený.")
  organizer3.skip_confirmation!
  organizer3.save!
end

unless User.exists?(email: "jedi@smartticket.sk")
  organizer4 = User.new(
    email: 'jedi@smartticket.sk',
    nickname: 'jedi',
    password: 'password',
    password_confirmation: 'password',
    name: 'Jedi',
    short_description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras aliquet volutpat dapibus. Sed at ante accumsan, semper arcu a, vulputate lorem. Integer condimentum luctus vulputate.',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras aliquet volutpat dapibus. Sed at ante accumsan, semper arcu a, vulputate lorem. Integer condimentum luctus vulputate. Pellentesque porttitor mattis interdum. Suspendisse finibus ullamcorper lorem vel fringilla. Morbi diam diam, eleifend ut erat vel, sodales aliquet leo. Nullam pellentesque, tortor hendrerit semper interdum, purus mauris pellentesque lacus, quis sodales nibh orci et dui. Donec molestie eu dolor efficitur dictum. Sed ac purus dolor. Morbi a erat id purus congue facilisis in ac diam. Etiam in gravida magna. Quisque fermentum elit interdum dolor suscipit feugiat. Nunc auctor nunc eu ante bibendum, pulvinar cursus metus tincidunt. Quisque vel lorem lobortis, volutpat turpis sit amet, feugiat neque.',
    avatar: File.open("#{Rails.root}/img/jedi.png"),
    web: "http://www.jedi.tt",
    address: "Tatooine 22",
    zip: "99074",
    city: "Ken Boi",
    country: country_sk,
    accepted: true,
    role: :organizer
  )
  organizer4.create_accept_message(content: "Váš profil bol schválený.")
  organizer4.skip_confirmation!
  organizer4.save!
end

unless User.exists?(email: "satriani@smartticket.sk")
  satriani = User.new(
    email: 'satriani@smartticket.sk',
    password: 'password',
    password_confirmation: 'password',
    name: 'Joe Satriani',
    short_description: 'Joe Satriani is an American instrumental rock guitarist and multi-instrumentalist. Early in his career, Satriani worked as a guitar instructor, with many of his former students achieving fame, such as Steve Vai, Larry LaLonde, Rick Hunolt, Kirk Hammett, Andy Timmons, Charlie Hunter, Kevin Cadogan, and Alex Skolnick; he then went on to have a successful solo music career. He is a 15-time Grammy Award nominee and has sold over 10 million albums.',
    description: 'In 1988, Satriani was recruited by Mick Jagger as lead guitarist for his first solo tour. In 1994, Satriani toured with Deep Purple as the lead guitarist. He has worked with a range of guitarists during the G3 tour, which he founded in 1995. His G3 collaborators have included Vai, LaLonde, Timmons, Steve Lukather, John Petrucci, Eric Johnson, Yngwie Malmsteen, Brian May, Patrick Rondat, Paul Gilbert, Adrian Legg, Kenny Wayne Shepherd, Steve Morse and Robert Fripp. Satriani has been the lead guitarist for the supergroup Chickenfoot since co-founding the band in 2008. Since 1988, he has been using his own signature guitars, the Ibanez JS Series, which are sold in music stores worldwide. He has also collaborated with Vox to create his own wah, delay, overdrive and distortion pedals as well as a collaboration with Marshall Amplification for the creation of his own signature series amplifier head, the JVM410HJS.',
    avatar: File.open("#{Rails.root}/img/satriani.jpg"),
    web: "http://www.satriani.com",
    address: "Smartticket 10",
    zip: "09112",
    city: "Chicagolado",
    country: country_us,
    artist_type: artist_type_musician,
    role: :artist
  )
  satriani.likecategoryships.build(category: Category.find_by_name("rock"))
  satriani.skip_confirmation!
  satriani.save!
end

unless User.exists?(email: "petrucci@smartticket.sk")
  petrucci = User.new(
    email: 'petrucci@smartticket.sk',
    password: 'password',
    password_confirmation: 'password',
    name: 'John Petrucci',
    short_description: 'John Petrucci is best known as the Grammy nominated guitarist and founding member of the progressive metal band Dream Theater. He is also the band\'s producer and main lyricist as well as an original member of the acclaimed Liquid Tension Experiment with Tony Levin. John is a long standing veteran of Joe Satriani\'s prestigious G3 tours along with Steve Vai, Eric Johnson and Paul Gilbert.',
    description: 'Born John Peter Petrucci on July 12, 1967, he grew up in Kings Park, New York, a small suburban town on Long Island. John began playing guitar at age 12 and quickly realized that music was his passion and that the pursuit of excellence on his instrument would consume him for the years to come. To that end, he practiced for at least 6 hours a day in an effort to further his understanding and technical abilities as a guitar player. Some of his early influences include Steve Morse, Al DiMeola, Steve Howe, Allan Holdsworth, Stevie Ray Vaughan, Randy Rhoads, Joe Satriani, Steve Vai, Alex Lifeson, Yngwie Malmsteen, Rush, Yes, Iron Maiden, The Dregs and Metallica. At age 18 after graduating from high school, he attended the Berklee College of Music in Boston along with schoolmate/bassist John Myung where they fatefully met drummer Mike Portnoy and quickly formed the nucleus of what was to become Dream Theater.',
    avatar: File.open("#{Rails.root}/img/petrucci.jpg"),
    web: "http://www.johnpetrucci.com",
    address: "Smartticket 10",
    zip: "09112",
    city: "New Yorkshire",
    country: country_us,
    artist_type: artist_type_musician,
    role: :artist
  )
  petrucci.likecategoryships.build(category: Category.find_by_name("rock"))
  petrucci.skip_confirmation!
  petrucci.save!
end

unless User.exists?(email: "vai@smartticket.sk")
  vai = User.new(
    email: 'vai@smartticket.sk',
    password: 'password',
    password_confirmation: 'password',
    name: 'Steve Vai',
    short_description: 'Steven Siro Vai (born June 6, 1960) is an American guitarist, songwriter, singer, and producer who has sold over 15 million albums. After starting his career as a music transcriptionist for Frank Zappa, he recorded and toured in Zappa\'s band from 1980 to 1982. He began a solo career in 1983, and has released eight solo albums and won three Grammy Awards.',
    description: 'Vai has recorded and toured with Public Image Ltd., Alcatrazz, David Lee Roth, and Whitesnake. He has been a regular touring member of the G3 Concert Tour, which began in 1995. In 1999, he started his own record label, Favored Nations, intending to showcase "artists that have attained the highest performance level on their chosen instruments".',
    avatar: File.open("#{Rails.root}/img/vai.jpg"),
    web: "http://www.vai.com",
    address: "Smartticket 10",
    zip: "09112",
    city: "Chicagolado",
    country: country_us,
    artist_type: artist_type_musician,
    role: :artist
  )
  vai.likecategoryships.build(category: Category.find_by_name("rock"))
  vai.skip_confirmation!
  vai.save!
end

unless User.exists?(email: "g3@smartticket.sk")
  g3 = User.new(
    email: 'g3@smartticket.sk',
    password: 'password',
    password_confirmation: 'password',
    name: 'G3',
    short_description: 'G3 (G4 from 2015) is a concert tour organized by guitarist Joe Satriani featuring him alongside two other guitarists.',
    description: 'Since its inception in 1995, G3 has toured most years and has featured many guitarists, including Steve Vai (Satriani\'s former student), Eric Johnson, Kenny Wayne Shepherd, Yngwie Malmsteen, John Petrucci, Robert Fripp, Paul Gilbert, Steve Morse, Steve Lukather, Uli Jon Roth, Michael Schenker, Adrian Legg and many other special guests, including Tony MacAlpine, Johnny Hiland, Keith More, Chris Duarte, Andy Timmons, Neal Schon, Gary Hoey, Brian May, Billy Gibbons, Johnny A, George Lynch, Patrick Rondat, Herman Li, Alejandro Silva, Eric Sardinas, and Justin Guerin.',
    avatar: File.open("#{Rails.root}/img/g3.jpg"),
    web: "http://www.g3.sk",
    address: "Smartticket 10",
    zip: "09112",
    city: "Chicagolado",
    country: country_us,
    artist_type: artist_type_band,
    role: :artist
  )
  g3.likecategoryships.build(category: Category.find_by_name("rock"))
  g3.memberships.build(member: satriani)
  g3.memberships.build(member: vai)
  g3.memberships.build(member: petrucci)
  g3.skip_confirmation!
  g3.save!
end

unless User.exists?(email: "aldimeola@smartticket.sk")
  aldimeola = User.new(
    email: 'aldimeola@smartticket.sk',
    password: 'password',
    password_confirmation: 'password',
    name: 'Al Di Meola',
    short_description: 'Al Di Meola (born Al Laurence Dimeola, July 22, 1954 in Jersey City, New Jersey) is an acclaimed American jazz fusion and Latin jazz guitarist, composer, and record producer of Italian origin (from Cerreto Sannita). With a musical career that has spanned more than three decades, he has become respected as one of the most influential guitarists in jazz to date. Albums such as Friday Night in San Francisco have earned him both artistic and commercial success.',
    description: 'In 1971 Di Meola enrolled in Berklee College of Music in Boston, Massachusetts. In 1974 he joined Chick Corea\'s band, Return to Forever for the album Where Have I Known You Before. Two more albums, No Mystery (1975) and Romantic Warrior (1976) were released during Di Meola\'s stay in the band. This lineup featured him playing with Corea, Stanley Clarke, and Lenny White until it was disbanded in 1976. Di Meola went on to explore a variety of styles, but is most noted for his Latin-influenced jazz fusion works. He is a four-time winner as Best Jazz Guitarist in Guitar Player Magazine\'s Reader Poll.',
    avatar: File.open("#{Rails.root}/img/meola.jpg"),
    web: "http://www.smartticket.sk",
    address: "Smartticket 10",
    zip: "09112",
    city: "Chicagolado",
    country: country_us,
    artist_type: artist_type_musician,
    role: :artist
  )
  aldimeola.likecategoryships.build(category: Category.find_by_name("fusion"))
  aldimeola.skip_confirmation!
  aldimeola.save!
end

unless User.exists?(email: "lizzwright@smartticket.sk")
  lizzwright = User.new(
    email: 'lizzwright@smartticket.sk',
    password: 'password',
    password_confirmation: 'password',
    name: 'Lizz Wright',
    short_description: 'Wright was born in the small town of Hahira in the US state of Georgia; one of three children and the daughter of a minister and the musical director of their Church. She started singing gospel music and playing piano in church as a child, and also became interested in jazz and blues. She attended Houston County High School, where she was heavily involved in choral singing, receiving the National Choral Award.',
    description: 'Her first album, Salt, was released in the spring of 2003 and reached number two on the Billboard Top Contemporary Jazz chart in 2004. Her next release was not a follow-up of her debut, but this record maintained the jazz and pop blend, while incorporating folk music to her musical blend. Dreaming Wide Awake was released in June 2005 and reached number one on the Top Contemporary Jazz chart in 2005 and 2006. In 2008, Wright released The Orchard to positive reviews. She released her fourth album in 2010. Most songs on Fellowship are gospel standards.',
    avatar: File.open("#{Rails.root}/img/wright.jpg"),
    web: "http://www.lizzwright.net",
    address: "Smartticket 10",
    zip: "09112",
    city: "Chicagolado",
    country: country_us,
    artist_type: artist_type_musician,
    role: :artist
  )
  lizzwright.likecategoryships.build(category: Category.find_by_name("jazz"))
  lizzwright.likecategoryships.build(category: Category.find_by_name("soul_jazz"))
  lizzwright.skip_confirmation!
  lizzwright.save!
end

unless User.exists?(email: "richardbona@smartticket.sk")
  richardbona = User.new(
    email: 'richardbona@smartticket.sk',
    password: 'password',
    password_confirmation: 'password',
    name: 'Richard Bona',
    short_description: 'Bona was born into a family of musicians, which enabled him to start learning music from a young age. His grandfather was a griot – a West African singer of praise and storyteller – and percussionist, and his mother was a singer. At 4 years old, Bona started to play the balafon. At the age of 5, he began performing at his village church. Not being wealthy, Bona made many of his own instruments: including flutes and guitars (with cords strung over an old motorcycle tank).',
    description: 'His talent was quickly noticed, and he was often invited to perform at festivals and ceremonies. Bona began learning to play the guitar at age 11, and in 1980 aged just 13, he assembled his first ensemble for a French jazz club in Douala. The owner befriended him and helped him discover jazz music, in particular that of Jaco Pastorius, which inspired Bona to switch his focus to the electric bass.',
    avatar: File.open("#{Rails.root}/img/bona.jpg"),
    web: "http://www.bonamusic.com",
    address: "Smartticket 10",
    zip: "09112",
    city: "Chicagolado",
    country: country_us,
    artist_type: artist_type_musician,
    role: :artist
  )
  richardbona.likecategoryships.build(category: Category.find_by_name("jazz"))
  richardbona.skip_confirmation!
  richardbona.save!
end

unless User.exists?(email: "suziquatro@smartticket.sk")
  suziquatro = User.new(
    email: 'suziquatro@smartticket.sk',
    password: 'password',
    password_confirmation: 'password',
    name: 'Suzi Quatro',
    short_description: 'From an early age she studied classical piano and percussion, then aged 14, began an all girl band with elder sister Patti, called "The Pleasure Seekers". Suzi was ‘told’ she would be playing bass guitar, which was as tall as her! she a quickly became the lead singer and front person, this band toured for 7 years, very successfully, releasing a few singles along the way ,then changed their name to cradle in 1969.',
    description: 'Soon after Suzi began working with  now legendary songwriters Chinn and Chapman (Mike Chapman also produced) , which resulted the huge hit, “Can The Can”, which went to number one in May 1973, and went on to sell two and a half million copies worldwide. Between 1973 and 1980, Suzi Quatro featured in the British charts for no less than 101 weeks, and has sold to date over 55 million records, and still counting. her hits include, “48 Crash”, “Too Big”, “Devil Gate Drive”, “Daytona Demon”, “The Wild One”, “She’s in Love With You”, “Mama’s Boy”, “If You Can’t Give Me Love”, “Rock Hard”,  and a million seller in the USA., ‘Stumblin In’ a duet with Chris Norman .',
    avatar: File.open("#{Rails.root}/img/quatro.jpg"),
    web: "http://www.suziquatro.com",
    address: "Smartticket 10",
    zip: "09112",
    city: "Chicagolado",
    country: country_us,
    artist_type: artist_type_musician,
    role: :artist
  )
  suziquatro.likecategoryships.build(category: Category.find_by_name("pop"))
  suziquatro.likecategoryships.build(category: Category.find_by_name("rock"))
  suziquatro.skip_confirmation!
  suziquatro.save!
end

unless User.exists?(email: "johnpotter@smartticket.sk")
  johnpotter = User.new(
    email: 'johnpotter@smartticket.sk',
    password: 'password',
    password_confirmation: 'password',
    name: 'John Potter',
    short_description: 'Ex-Hilliard Ensemble tenor John Potter is a performer, author and vocal coach.',
    description: 'He records for ECM and Hyperion and his current performance projects include new programmes with lute, vihuela and harp, electronics and video as well as his work with The Dowland Project, Red Byrd and the Gavin Bryars Ensemble. After 17 years with the Hilliard Ensemble and a period as a lecturer at the University of York, he returned to freelance performing and writing in 2010. His latest book from Cambridge University Press is A History of Singing, written jointly with ethnomusicologist Neil Sorrell. This site gives details of his current activities; the News page also features the occasional blog post.',
    web: "http://www.john-potter.co.uk",
    address: "Smartticket 10",
    zip: "09112",
    city: "Chicagolado",
    country: country_us,
    artist_type: artist_type_musician,
    role: :artist
  )
  johnpotter.likecategoryships.build(category: Category.find_by_name("classical_music"))
  johnpotter.skip_confirmation!
  johnpotter.save!
end

unless User.exists?(email: "rogerscoveycrump@smartticket.sk")
  rogerscoveycrump = User.new(
    email: 'rogerscoveycrump@smartticket.sk',
    password: 'password',
    password_confirmation: 'password',
    name: 'Rogers Covey-Crump',
    short_description: 'The English tenor, Rogers Covey-Crump, was a boy chorister at New College, Oxford and later a tenor lay-clerk at St Albans Abbey. While studying at the Royal College of Music in London he gained diplomas and a prize in organ playing. He graduated from London University as a Bachelor of Music.',
    description: 'Rogers Covey-Crump\'s work as a singer soon overtook his keyboard playing and for many years he worked with a number of early music ensembles and in particular with the late David Munrow\'s Early Music Consort of London, the Baccholian Singers, the Consort of Musicke, the Medieval Ensemble of London, and the Landini, Deller and Taverner Consorts. As a member of Gregory Rose\'s Singcircle he gave the first performances and recording of Stockhausen\'s Stimmung by a British group. Although a founder member of Gothic Voices in 1981 his main activity is as a member of the four-voice Hilliard Ensemble. Aside from an annual schedule of up to one hundred concerts the Hilliard Ensemble conducted its own Summer School and Festival in Cambridge for several years. In 2000 the School relocated to Germany. After many years of recording for EMI, Harmonia Mundi and Hyperion recent recordings have been for the Munich-based label ECM whose catalogue is mainly devoted to the work of composer- and jazz-artists. The invitation, in 1993, to collaborate with the Norwegian saxophonist, Jan Garbarek, resulted in the album Officium. The success of this venture has produced an ongoing demand for the collaboration and a second album, Mnemosyne, was released in the Spring of 1999.',
    web: "http://www.john-potter.co.uk",
    address: "Smartticket 10",
    zip: "09112",
    city: "Chicagolado",
    country: country_us,
    artist_type: artist_type_musician,
    role: :artist
  )
  rogerscoveycrump.likecategoryships.build(category: Category.find_by_name("classical_music"))
  rogerscoveycrump.skip_confirmation!
  rogerscoveycrump.save!
end

unless User.exists?(email: "christopherogorman@smartticket.sk")
  christopherogorman = User.new(
    email: 'christopherogorman@smartticket.sk',
    password: 'password',
    password_confirmation: 'password',
    name: 'Christopher O\'Gorman',
    short_description: 'Christopher O’Gorman studied at the Junior Department of the Royal Academy of Music and then the University of York where he gained an MA in vocal studies with John Potter. He now pursues a career both as a Songman in the Choir of York Minster and as an ensemble singer with a number of groups including the Gabrieli Consort, The Binchois Consort, The London Quartet, iFagiolini, Ex Cathedra, Britten Sinfonia Voices, The Brabant Ensemble and The Ebor Singers.',
    description: 'As a soloist, Christopher is an active performer of medieval music. He is currently working with the musicologist Mark Everist, and tenors John Potter and Rogers Covey-Crump on a project entitled Cantum pulcriorem invenire, which will culminate in three recordings for Hyperion Records. The first recording, Conductus Vol I was released in September 2012 and was received enthusiastically by the musical press. Gramophone Magazine said that ‘…[John Potter] is magically balanced by the glorious voice of Christopher O’Gorman’. The second recording was released in December 2013.',
    avatar: File.open("#{Rails.root}/img/ogorman.jpg"),
    web: "http://www.john-potter.co.uk",
    address: "Smartticket 10",
    zip: "09112",
    city: "Chicagolado",
    country: country_us,
    artist_type: artist_type_musician,
    role: :artist
  )
  christopherogorman.likecategoryships.build(category: Category.find_by_name("classical_music"))
  christopherogorman.skip_confirmation!
  christopherogorman.save!
end

unless User.exists?(email: "solo@smartticket.sk")
  visitor1 = User.new(
    email: 'solo@smartticket.sk',
    nickname: 'solo',
    show_real_name: true,
    password: 'password',
    password_confirmation: 'password',
    name: 'Han',
    surname: 'Solo',
    avatar: File.open("#{Rails.root}/img/solo.jpg"),
    sex: :male,
    date_of_birth: Date.strptime("04/08/1975", "%d/%m/%Y"),
    city: "Bratislava",
    country: country_sk,
    role: :user
  )
  visitor1.likecategoryships.build(category: Category.find_by_name("music"))
  visitor1.inverse_fanships.build(user: vai)
  visitor1.inverse_fanships.build(user: satriani)
  visitor1.skip_confirmation!
  visitor1.save!
end

unless User.exists?(email: "skywalker@smartticket.sk")
  visitor2 = User.new(
    email: 'skywalker@smartticket.sk',
    nickname: 'skywalker',
    show_real_name: true,
    password: 'password',
    password_confirmation: 'password',
    name: 'Luke',
    surname: 'Skywalker',
    avatar: File.open("#{Rails.root}/img/skywalker.jpeg"),
    sex: :male,
    date_of_birth: Date.strptime("05/01/1980", "%d/%m/%Y"),
    city: "Bratislava",
    country: country_sk,
    role: :user
  )
  visitor2.likecategoryships.build(category: Category.find_by_name("sport"))
  visitor2.likecategoryships.build(category: Category.find_by_name("rock"))
  visitor2.likecategoryships.build(category: Category.find_by_name("swing"))
  visitor2.inverse_fanships.build(user: vai)
  visitor2.inverse_fanships.build(user: satriani)
  visitor2.inverse_fanships.build(user: organizer2)
  visitor2.inverse_fanships.build(user: organizer4)
  visitor2.skip_confirmation!
  visitor2.save!lizzwright
end

unless User.exists?(email: "kenobi@smartticket.sk")
  visitor3 = User.new(
    email: 'kenobi@smartticket.sk',
    nickname: 'kenobi',
    show_real_name: true,
    password: 'password',
    password_confirmation: 'password',
    name: 'Obi-Wan',
    surname: 'Kenobi',
    avatar: File.open("#{Rails.root}/img/kenobi.jpg"),
    sex: :male,
    date_of_birth: Date.strptime("09/03/1948", "%d/%m/%Y"),
    city: "Bratislava",
    country: country_sk,
    role: :user
  )
  visitor3.likecategoryships.build(category: Category.find_by_name("theater"))
  visitor3.likecategoryships.build(category: Category.find_by_name("music"))
  visitor3.inverse_fanships.build(user: petrucci)
  visitor3.inverse_fanships.build(user: vai)
  visitor3.inverse_fanships.build(user: organizer2)
  visitor3.inverse_fanships.build(user: organizer4)
  visitor3.skip_confirmation!
  visitor3.save!
end

unless User.exists?(email: "joda@smartticket.sk")
  visitor4 = User.new(
    email: 'joda@smartticket.sk',
    nickname: 'joda',
    show_real_name: true,
    password: 'password',
    password_confirmation: 'password',
    name: 'Majster',
    surname: 'Joda',
    avatar: File.open("#{Rails.root}/img/joda.jpeg"),
    sex: :male,
    date_of_birth: Date.strptime("06/01/1898", "%d/%m/%Y"),
    city: "Bratislava",
    country: country_sk,
    role: :user
  )
  visitor4.likecategoryships.build(category: Category.find_by_name("auto_moto"))
  visitor4.likecategoryships.build(category: Category.find_by_name("drama"))
  visitor4.likecategoryships.build(category: Category.find_by_name("jazz"))
  visitor4.inverse_fanships.build(user: vai)
  visitor4.inverse_fanships.build(user: satriani)
  visitor4.inverse_fanships.build(user: petrucci)
  visitor4.inverse_fanships.build(user: organizer4)
  visitor4.skip_confirmation!
  visitor4.save!
end

unless User.exists?(email: "leia@smartticket.sk")
  visitor5 = User.new(
    email: 'leia@smartticket.sk',
    nickname: 'leia',
    show_real_name: true,
    password: 'password',
    password_confirmation: 'password',
    name: 'Princezná',
    surname: 'Leia',
    avatar: File.open("#{Rails.root}/img/leia.jpg"),
    sex: :female,
    date_of_birth: Date.strptime("05/01/1980", "%d/%m/%Y"),
    city: "Bratislava",
    country: country_sk,
    role: :user
  )
  visitor5.likecategoryships.build(category: Category.find_by_name("sport"))
  visitor5.likecategoryships.build(category: Category.find_by_name("music"))
  visitor5.inverse_fanships.build(user: vai)
  visitor5.inverse_fanships.build(user: satriani)
  visitor5.inverse_fanships.build(user: petrucci)
  visitor5.inverse_fanships.build(user: organizer2)
  visitor5.inverse_fanships.build(user: organizer4)
  visitor5.skip_confirmation!
  visitor5.save!
end

unless User.exists?(email: "vader@smartticket.sk")
  visitor6 = User.new(
    email: 'vader@smartticket.sk',
    nickname: 'vader',
    show_real_name: true,
    password: 'password',
    password_confirmation: 'password',
    name: 'Darth',
    surname: 'Vader',
    avatar: File.open("#{Rails.root}/img/vader.jpg"),
    sex: :male,
    date_of_birth: Date.strptime("20/11/1956", "%d/%m/%Y"),
    city: "Bratislava",
    country: country_sk,
    role: :user
  )
  visitor6.likecategoryships.build(category: Category.find_by_name("music"))
  visitor6.likecategoryships.build(category: Category.find_by_name("rock"))
  visitor6.likecategoryships.build(category: Category.find_by_name("jazz"))
  visitor6.inverse_fanships.build(user: vai)
  visitor6.inverse_fanships.build(user: satriani)
  visitor6.inverse_fanships.build(user: organizer1)
  visitor6.skip_confirmation!
  visitor6.save!
end


####################
# CREATE EVENTS
####################
unless Event.exists?(title: "Hudobné večery 5 - Three Medieval Tenors")
  event = Event.new(user: organizer1,
                title: "Hudobné večery 5 - Three Medieval Tenors",
                subtitle: "Tri Stredoveké tenory: Conductus – Zabudnutá pieseň stredoveku",
                title_image: File.open("#{Rails.root}/img/tenors.jpg"),
                category: Category.find_by_name("classical_music"),
                venue: Venue.find_by_name("Dóm sv. Martina"),
                start_date: Date.strptime("31/12/2016", "%d/%m/%Y"),
                start_time: Time.parse("20:00"),
                group: false,
                private: false,
                tag_list: "klasická hudba, Hilliard Ensemble, stará hudba, stredoveká hudba, vokálna hudba",
                video: "https://youtu.be/C9irlZNrwrA",
                short_description: "The English tenor, Rogers Covey-Crump, was a boy chorister at New College, Oxford and later a tenor lay-clerk at St Albans Abbey. While studying at the Royal College of Music in London he gained diplomas and a prize in organ playing. He graduated from London University as a Bachelor of Music.",
                description: "Koncert tenoristov **Johna Pottera** (vystúpil na festivale Konvergencie v projektoch *Being Dufay* a *The Dowland Project*), **Christophera O’Gormana** a **Rogersa Coveyho-Crumpa** (vystúpil na festivale Konvergencie spolu s **The Hilliard Ensemble**) je fascinujúcim návratom do veku katedrál v 13. storočí. Sprievodcom na tomto putovaní je conductus, unikátne spojenie stredovekej hudby a poézie, ktorej texty rozprávajú príbehy o Bohu a láske, ale i o smrti a neprávostiach doby. Séria nahrávok dvojice bývalých členov The Hilliard Ensemble a Christophera O’Gormana pre prestížny label Hyperion sa stretla s pozitívnymi ohlasmi renomovaných britských mesačníkov o klasickej hudbe *Gramophone* („classy performances“) a *BBC Music Magazine* („it’s hardly necessary to mention that the performances are superb“).

                **Interpreti:**

                - John POTTER (UK)
                - Rogers COVEY-CRUMP (UK)
                - Christopher O’GORMAN (UK)",

                gallery_images: [
                   GalleryImage.create!({
                     gallery_image: File.new("#{Rails.root}/img/tenors2.jpg"),
                     description: "Ujovia špivači čiernobielo"
                   }),
                   GalleryImage.create!({
                     gallery_image: File.new("#{Rails.root}/img/tenors3.jpg"),
                     description: "Ujovia špivači"
                   }),
                   GalleryImage.create!({
                     gallery_image: File.new("#{Rails.root}/img/tenors4.jpg"),
                     description: "Špivaju"
                   }),
                   GalleryImage.create!({
                     gallery_image: File.new("#{Rails.root}/img/tenors5.jpg"),
                     description: "Pekne špivaju"
                   })
                 ])
  event.performships.build(user: johnpotter)
  event.performships.build(user: rogerscoveycrump)
  event.performships.build(user: christopherogorman)
  event.save!
end

unless Event.exists?(title: "Al Di Meola - Beatles & More")
  event = Event.new(user: organizer1,
                title: "Al Di Meola - Beatles & More",
                title_image: File.open("#{Rails.root}/img/dimeola.jpeg"),
                category: Category.find_by_name("crossover_jazz"),
                venue: Venue.find_by_name("Stará tržnica"),
                start_date: Date.strptime("12.09.2016", "%d.%m.%Y"),
                start_time: Time.parse("20:00"),
                group: true,
                private: false,
                tag_list: "Beatles, cover, flamenco, jazz",
                video: "https://youtu.be/C9irlZNrwrA",
                short_description: "Al Di Meola, jeden z najlepších gitaristov sveta s výnimočným projektom BEATLES & MORE v Bratislave! Počas svojej 40 ročnej hudobnej kariéry vydal viac ako 20 albumov.",
                description: "Spolupracoval s osobnosťami svetovej hudby ako sú Chick Corea, Stanley Clarke, Lenny White, John McLaughin, Paco De Lucia, Jaco Pastorius či Jean-Luc Ponty a mnohými ďalšími. Je držiteľom ceny Grammy, doteraz predal viac ako 6 miliónov albumov po celom svete. Jeho meno bolo už v roku 1981 slávnostne zaradené do siene slávy Guitar Player's Gallery Of Greats. Kým jeho doterajšie vystúpenia na Slovensku boli tak povediac tradičné a predstavil na nich hlavne svoje jazzové či flamengo projekty, tentokrát si pre svojich fanúšikov pripravil skutočnú koncertnú lahôdku. Al Di Meola odohrá v Bratislave 9.júna 2015 špeciálny koncert pod názvom Beatles & More. Na koncerte zaznejú reinterpretácie 14 známych melódií The Beatles v prepracovanom virtuóznom aranžmá upravenom špeciálne pre akustickú gitaru. Doprevádzať ho budú **Peo Alfonsi** (2nd guitar) a **Peter Kaszas** (drums and percussion). Veľký obdivovateľ The Beatles nahral v roku 2013 tieto nadčasové skladby na svoj album *All Your Life* priamo v štúdiách Abbey Road v Londýne a odvtedy ich prináša aj na koncertné pódiá.",
                gallery_images: [
                   GalleryImage.create!({
                     gallery_image: File.new("#{Rails.root}/img/dimeola2.jpg"),
                     description: "Testovaci popis"
                   }),
                   GalleryImage.create!({
                     gallery_image: File.new("#{Rails.root}/img/dimeola3.jpg"),
                     description: "Testovaci popis"
                   }),
                 ])
  event.performships.build(user: aldimeola)
  event.save!
end

unless Event.exists?(title: "Lizz Wright /USA/")
  event = Event.new(user: organizer2,
                title: "Lizz Wright /USA/",
                title_image: File.open("#{Rails.root}/img/lizzwright.gif"),
                category: Category.find_by_name("soul_jazz"),
                venue: Venue.find_by_name("Ateliér Babylon"),
                start_date: Date.strptime("31/12/2016", "%d/%m/%Y"),
                start_time: Time.parse("20:00"),
                group: false,
                private: false,
                tag_list: "jazz, blues, gospel, soul, Katarzia",
                video: "https://youtu.be/C9irlZNrwrA",
                short_description: "Tridsiatnička Lizz Wright je napriek svojmu veku už niekoľko rokov hviezdou súčasného amerického jazzu.",
                description: "Už ako dieťa začala spievať gospel a hrať na klavíri, spievala v zbore a na strednej škole objavila jazz a blues. Spev potom študovala na prestížnych univerzitách v Atlante, New Yorku a Vancouveri. V roku 2002 podpísala nahrávaciu zmluvu s legendárnou značkou Verve Records a stala sa súčasťou najexkluzívnejšieho katalógu a hneď získala priazeň publika aj u kritikov. Jej albumy sa umiestňujú na Billboard Contemporary Jazz Chart - posledné dva na prvom mieste. Lizz nielen spieva, ale je aj autorkou časti svojho repertoáru. Producentom jej aktuálneho albumu *The Orchard* je Craig Street, ktorý spolupracoval s Cassandrou Wilson, Norah Jones či Me´shell Ndegéocello a pravdou je, že s týmito modernými pesničkami viac či menej strihnutými jazzom ju spája veľa. V prípade Cassandry aj hlbší, zamatový hlas a vláčne frázovanie.

                Pred vystúpením Lizz Wright publikum rozohreje svieži slovenský projekt Katarzia, ktorý od svojho relatívne nedávneho vstupu na scénu rozvíril miestne hudobné vody neošúchaným prejavom, zaujímavými textami a v neposlednom rade fascinujúcim zjavom: mladučká **Katarína Kubošiová** skutočne vie, ako vizuálne zaujať.
                Pritom je však jasné, že jej žiarivosť nie je žiadnym samoúčelným pútaním pozornosti. Katarínin nespochybniteľný pesničkársky talent rýchlo zatieni aj akokoľvek excentrické outfity.
                Katarína je samouk a to jej dodáva odvahu nebyť dokonalou; je všelijaká, no zaručene nie nudná. Prvé vystúpenie absolvovala len so štyrmi pesničkami, dnes ich na konte nemá oveľa viac, no stala sa neuveriteľne žiadanou na malých koncertoch v kaviarňach či baroch, aj na väčších festivaloch. Surové, no hravé texty sú spievané či polohovorené detsky naturálnym hlasom, pričom pokrývajú témy od nešťastnej lásky až po vážne existencionálne otázky.
                Katarzia je vždy absolútne strhujúca, nedá sa nepočúvať a neobdivovať, je to zjav, ktorý sa oplatí sledovať. Ak ste ju ešte nazažili naživo, máte sa naozaj na čo tešiť.

                - 19.00 - Katarzia
                - 20.00 - Lizz Wright",
                gallery_images: [
                   GalleryImage.create!({
                     gallery_image: File.new("#{Rails.root}/img/katarzia.jpg"),
                     description: "Katarzia"
                   }),
                 ])
  event.performships.build(user: lizzwright)
  event.save!
end

unless Event.exists?(title: "Suzi Quatro")
  event = Event.new(user: organizer2,
                title: "Suzi Quatro",
                title_image: File.open("#{Rails.root}/img/suzi.jpg"),
                category: Category.find_by_name("pop"),
                venue: Venue.find_by_name("AEGON aréna"),
                start_date: Date.strptime("24/08/2016", "%d/%m/%Y"),
                start_time: Time.parse("19:00"),
                group: false,
                private: false,
                tag_list: "pop",
                video: "https://youtu.be/C9irlZNrwrA",
                short_description: "Priekopníčka ženského rocku Suzi Quatro vystúpi v Bratislave v rámci posledného svetového turné 50. ročnej kariéry! Legendárna speváčka Suzi Quatro mieri na Slovensko! Prvá dáma svetového rocku vystúpi 19. decembra v bratislavskej Aegon Aréne NTC, aby oslávila s fanúšikmi 50. rokov muzikantskej kariéry.",
                description: "Slováci majú poslednú šancu vychutnať si naživo najväčšie hity rockovej legendy. Suzi Quatro totiž hovorí o poslednom svetovom turné a odchode do dôchodku. Nenechajte si ujsť rockové trháky ako „Can the Can“, „48 Crash“, „Devil Gate Drive“ či „She's In Love With You“!

                Milovníci rockovej hudby na Slovensku majú jedinečnú šancu zažiť dvojhodinový koncert Suzi Quatro plnýsubevent3.group = true hitov ako „Can the Can“, „48 Crash“, „Devil Gate Drive“ či „She's In Love With You“! V Aegon Aréne NTC odohrá oceňovaná hudobníčka prierez 50- ročnej tvorby.

                „Predvediem dvojhodinovú šou, na ktorej som tvrdo pracovala, bude to veľmi špeciálne. Nechcem veľa prezradiť, ale budete odchádzať so slzami v očiach.“ Povedala Suzi Quatro.

                Suzi Quatro je speváčka, basgitaristka, rozhlasová moderátorka a herečka s taliansko-maďarskými koreňmi. Narodila sa v Detroite a vyrastala v hudobníckej rodine. Prvú skupinu s názvom „Suzi Soul & The Pleasure Seekers“ založila ako 14-ročná so sestrami Patti, Nancy a Arlene. Talentovanú rockerku oslovilo americké vydavateľstvo Elektra Records, aby sa stala novou Janis Joplin. Suzi Quatro však odmietla zaplniť medzeru po zosnulej speváčke, chcela byť sama sebou. V roku 1970 začala Suzi pracovať na sólovej kariére s producentom Mickiem Mostom vo Veľkej Británii, čo jej prinieslo obrovský úspech. Suzi Quatro nahrala za svoju kariéry 16 hitov a predala viac ako 45 miliónov hudobných nahrávok. Na konte má 14 štúdiových albumov.

                Otvorenie brány: 18:00 hod.")
  event.performships.build(user: suziquatro)
  event.save!
end

unless Event.exists?(title: "Richard Bonas's Mandekan Cubano")
  event = Event.new(user: organizer3,
                title: "Richard Bonas's Mandekan Cubano",
                title_image: File.open("#{Rails.root}/img/rbona.jpg"),
                category: Category.find_by_name("jazz"),
                venue: Venue.find_by_name("Stará tržnica"),
                start_date: Date.strptime("24/10/2016", "%d/%m/%Y"),
                start_time: Time.parse("19:00"),
                group: false,
                private: false,
                tag_list: "jazz",
                video: "https://youtu.be/C9irlZNrwrA",
                short_description: "Basgitarista Richard Bona pochádza z Kamerunu, študoval a hral v Nemecku, Francúzsku, nakoniec sa usadil v New Yorku. Doprevádzal mnoho zvučných mien ako Mike Stern, Pat Metheny, Bobby McFerrin, Didier Lockwood, Joe Zawinul, Steve Gadd, Larry Coryell, Manu Dibango, Salif Keita, Mino Cinelu, Michael Brecker.",
                description: "**Richard Bona** bol dva roky hudobným riaditeľom projektov Harryho Belafonte, je stálym profesorom hudby na New York University. Jeho vlastná produkcia je svojským prienikom jazzu a rocku, inšpirovaná hudbou rodnej Západnej Afriky.

                Pozíciu svetovej hviezdy mu priniesol album *Tiki* v roku 2005. Rok nato sa predstavil na **Bratislavských Jazzových Dňoch Slovenská sporiteľňa**. V roku 2008 sa k nám vrátil ako headliner Jarných BJD. Odvtedy vydal dva úspešné albumy *The Ten Shades Of Blues* a *Bonafied*.

                **Richard Bona’s Mandekan Cubano** je plnokrvná afrokubánska jazzová kapela s vychytenými hráčmi, z ktorých je najslávnejším skladateľ a pianista **Osmany Paredes**.

                Paredes je medzinárodnou hviezdou kubánskej hudby, od začiatku deväťdesiatych rokov spolupracoval s plejádou amerických a mexických hráčov. Presťahoval sa do New Yorku, kde založil kapelu **Latin Jazz Group**. Aktívne s ňou koncertuje vo vychytených kluboch v USA, aj na medzinárodných festivaloch.

                Koncert kubánskeho projektu Richarda bonu bude v rámci **Jarných Bratislavských Jazzových Dní**. Pozrite si video Richard Bona's Mandekan Cubano.

                Lineup:

                - Richard Bona basa, spev
                - Osmany Paredes, klavír
                - Luisito Quintero, perkusie
                - Roberto Quintero, perkusie
                - Rey Alejandro, trombón
                - Dennis Hernandez, trúbka")
  event.performships.build(user: richardbona)
  event.save!
end

unless Event.exists?(title: "Festival Pohoda 2015")
  pohoda = Event.new(user: organizer1,
                title: "Festival Pohoda 2015",
                title_image: File.open("#{Rails.root}/img/pohoda.jpg"),
                category: Category.find_by_name("music"),
                venue: Venue.find_by_name("Letisko Trenčín"),
                start_date: Date.strptime("9.7.2016", "%d.%m.%Y"),
                start_time: Time.parse("17:30"),
                end_date: Date.strptime("11.7.2016", "%d.%m.%Y"),
                group: true,
                private: false,
                tag_list: "",
                video: "https://youtu.be/2B7zu7wYXOw",
                short_description: "Pohoda je multižánrový festival na ktorom sa alternatíva, indie, elektronika, world music a punk stretávajú s klasikou; popri literatúre, tanci, výtvarnom umení, filme a divadle. Festival vytvára jedinečný priestor na komunikáciu a zdieľanie myšlienok nádherných a rozmanitých kultúr. Pohoda sa špecializuje na spájanie zaujímavých kombinácií rôznych umelcov – napríklad počas ročníka 2014 nasledoval za Ukrajinským filharmonickým orchestrom Kraftwerk a ich neuveriteľná 3D show.",
                description: "Pohoda sa koná vždy druhý júlový víkend v Trenčíne. Areálom festivalu je bývalé vojenské letisko v údolí rieky Váh, obkolesené tromi pohoriami Západných Karpát. Z hľadiska logistiky i komfortu návštevníkov ide o ideálny, veľkoryso riešený priestor s limitovanou kapacitou 30 000 návštevníkov. Zabudnite na rady v stánkoch s jedlom, či na záchodoch, prehustené stanové tábory a pomalé presuny v dave – kapacita je stanovená tak, aby bol festival útulný a vzdušný, aby zodpovedal svojmu názvu. Pohodu založili Michal Kaščák a Mário Michna v roku 1997. Vznikla ako stretnutie priateľov na futbalovom ihrisku v Trenčíne. Na prvom ročníku hralo osem skupín na jednom pódiu. V roku 1998 sa festival prvýkrát sťahoval do areálu Pod Sokolicami, pribudla tanečná scéna a prvýkrát boli zastúpené aj neziskovky. V roku 1999 sa Pohoda stala dvojdňovým festivalom. V roku 2000 sa festival rozšíril aj na trenčianske výstavisko, počet scén sa zvýšil na 6, pribudla literatúra a vážna hudba.",
                gallery_images: [
                   GalleryImage.create!({
                     gallery_image: File.new("#{Rails.root}/img/pohoda_01.jpg"),
                     description: "Plán letiska"
                   }),
                   GalleryImage.create!({
                     gallery_image: File.new("#{Rails.root}/img/pohoda_02.jpg"),
                     description: "Obrázok 1"
                   }),
                   GalleryImage.create!({
                     gallery_image: File.new("#{Rails.root}/img/pohoda_03.jpg"),
                     description: "Obrázok 2"
                   })
                 ])
  pohoda.save!

  stvrtok = Event.new
  stvrtok.parent = pohoda
  stvrtok.group = true
  stvrtok.update(user: organizer1,
                 title: "Štvrtok",
                 start_date: Date.strptime("9/7/2016", "%d/%m/%Y"),
                 start_time: Time.parse("17:30 UTC"),
                 end_date: Date.strptime("10/7/2016", "%d/%m/%Y"),
                 end_time: Time.parse("1:00 UTC"))

  subsubevent1 = Event.new
  subsubevent1.parent = stvrtok
  subsubevent1.update(user: organizer1,
                      title: 'Matwe Drappenmadchenfeller (SK)',
                      venue: Venue.find_by_name("Red Bull Tour Bus"),
                      start_date: Date.strptime("9/7/2016", "%d/%m/%Y"),
                      start_time: Time.parse("17:30 UTC"),
                      end_date: Date.strptime("9/7/2016", "%d/%m/%Y"),
                      end_time: Time.parse("18:30 UTC"))

  subsubevent2 = Event.new
  subsubevent2.parent = stvrtok
  subsubevent2.update(user: organizer1,
                     title: 'Monikino Kino (SK/CZ)',
                     venue: Venue.find_by_name("Red Bull Tour Bus"),
                     start_date: Date.strptime("9/7/2016", "%d/%m/%Y"),
                     start_time: Time.parse("20:00 UTC"),
                     end_date: Date.strptime("9/7/2016", "%d/%m/%Y"),
                     end_time: Time.parse("21:00 UTC"))

  subsubevent3 = Event.new
  subsubevent3.parent = stvrtok
  subsubevent3.update(user: organizer1,
                     title: 'Bez Ladu a Skladu (SK)',
                     venue: Venue.find_by_name("Bažant Stage"),
                     start_date: Date.strptime("9/7/2016", "%d/%m/%Y"),
                     start_time: Time.parse("20:00 UTC"),
                     end_date: Date.strptime("9/7/2016", "%d/%m/%Y"),
                     end_time: Time.parse("21:00 UTC"))

  subsubevent4 = Event.new
  subsubevent4.parent = stvrtok
  subsubevent4.update(user: organizer1,
                     title: 'Para (SK)',
                     venue: Venue.find_by_name("Bažant Stage"),
                     start_date: Date.strptime("9/7/2016", "%d/%m/%Y"),
                     start_time: Time.parse("18:30 UTC"),
                     end_date: Date.strptime("9/7/2016", "%d/%m/%Y"),
                     end_time: Time.parse("20:00 UTC"))

  piatok = Event.new
  piatok.parent = pohoda
  piatok.group = true
  piatok.update(user: organizer1,
                  title: "Piatok",
                  start_date: Date.strptime("10/7/2016", "%d/%m/%Y"),
                  start_time: Time.parse("12:00 UTC"),
                  end_date: Date.strptime("11/7/2016", "%d/%m/%Y"),
                  end_time: Time.parse("6:00 UTC"))

  subsubevent5 = Event.new
  subsubevent5.parent = piatok
  subsubevent5.update(user: organizer1,
                     title: 'Swingless Jazz Ensemble (SK)',
                     venue: Venue.find_by_name("Bažant Stage"),
                     start_date: Date.strptime("10/7/2016", "%d/%m/%Y"),
                     start_time: Time.parse("12:00 UTC"),
                     end_date: Date.strptime("10/7/2016", "%d/%m/%Y"),
                     end_time: Time.parse("13:00 UTC"))

  subsubevent6 = Event.new
  subsubevent6.parent = piatok
  subsubevent6.update(user: organizer1,
                     title: 'Billy Barman (SK)',
                     venue: Venue.find_by_name("Bažant Stage"),
                     start_date: Date.strptime("10/7/2016", "%d/%m/%Y"),
                     start_time: Time.parse("14:00 UTC"),
                     end_date: Date.strptime("10/7/2016", "%d/%m/%Y"),
                     end_time: Time.parse("15:00 UTC"))

  subsubevent7 = Event.new
  subsubevent7.parent = piatok
  subsubevent7.update(user: organizer1,
                     title: 'Gonsofus (RU/HU)',
                     venue: Venue.find_by_name("Orange Stage"),
                     start_date: Date.strptime("9/7/2016", "%d/%m/%Y"),
                     start_time: Time.parse("13:00 UTC"),
                     end_date: Date.strptime("10/7/2016", "%d/%m/%Y"),
                     end_time: Time.parse("14:00 UTC"))

  subsubevent8 = Event.new
  subsubevent8.parent = piatok
  subsubevent8.update(user: organizer1,
                    title: 'Medial Banana (SK)',
                    venue: Venue.find_by_name("Orange Stage"),
                    start_date: Date.strptime("10/7/2016", "%d/%m/%Y"),
                    start_time: Time.parse("15:00 UTC"),
                    end_date: Date.strptime("10/7/2016", "%d/%m/%Y"),
                    end_time: Time.parse("16:00 UTC"))

  sobota = Event.new
  sobota.parent = pohoda
  sobota.group = true
  sobota.update(user: organizer1,
                  title: "Sobota",
                  start_date: Date.strptime("11/7/2016", "%d/%m/%Y"),
                  start_time: Time.parse("12:00 UTC"),
                  end_date: Date.strptime("12/7/2016", "%d/%m/%Y"),
                  end_time: Time.parse("6:00 UTC"))

  subsubevent9 = Event.new
  subsubevent9.parent = sobota
  subsubevent9.update(user: organizer1,
                     title: 'break dance battle',
                     venue: Venue.find_by_name("Space Aréna SLSP"),
                     start_date: Date.strptime("11/7/2016", "%d/%m/%Y"),
                     start_time: Time.parse("11:00 UTC"),
                     end_date: Date.strptime("11/7/2016", "%d/%m/%Y"),
                     end_time: Time.parse("13:00 UTC"))

  subsubevent10 = Event.new
  subsubevent10.parent = sobota
  subsubevent10.update(user: organizer1,
                    title: 'Purist (SK)',
                    venue: Venue.find_by_name("Space Aréna SLSP"),
                    start_date: Date.strptime("9/7/2016", "%d/%m/%Y"),
                    start_time: Time.parse("15:00 UTC"),
                    end_date: Date.strptime("9/7/2016", "%d/%m/%Y"),
                    end_time: Time.parse("16:00 UTC"))

  subsubevent11 = Event.new
  subsubevent11.parent = sobota
  subsubevent11.update(user: organizer1,
                    title: 'Pišta Vandal: Obludárium',
                    venue: Venue.find_by_name("Európa Stage"),
                    start_date: Date.strptime("11/7/2016", "%d/%m/%Y"),
                    start_time: Time.parse("10:00 UTC"),
                    end_date: Date.strptime("11/7/2016", "%d/%m/%Y"),
                    end_time: Time.parse("11:00 UTC"))

  subsubevent12 = Event.new
  subsubevent12.parent = sobota
  subsubevent12.update(user: organizer1,
                    title: 'Café Európa: TEDx Bratislava: (ne)Všední Hrdinovia',
                    venue: Venue.find_by_name("Európa Stage"),
                    start_date: Date.strptime("11/7/2016", "%d/%m/%Y"),
                    start_time: Time.parse("12:00 UTC"),
                    end_date: Date.strptime("11/7/2016", "%d/%m/%Y"),
                    end_time: Time.parse("12:45 UTC"))

  stvrtok.update(subevents: [subsubevent1, subsubevent2, subsubevent3, subsubevent4])
  piatok.update(subevents: [subsubevent5, subsubevent6, subsubevent7, subsubevent8])
  sobota.update(subevents: [subsubevent9, subsubevent10, subsubevent11, subsubevent12])

  pohoda.update(subevents: [stvrtok, piatok, sobota])
end
