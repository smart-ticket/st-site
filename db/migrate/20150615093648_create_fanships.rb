class CreateFanships < ActiveRecord::Migration
  def change
    create_table :fanships do |t|
      t.integer :user_id
      t.integer :fan_id
    end
  end
end
