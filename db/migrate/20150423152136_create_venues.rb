class CreateVenues < ActiveRecord::Migration
  def change
    create_table :venues do |t|
      t.string :name
      t.string :street
      t.integer :zip
      t.string :city
      t.float :lat
      t.float :lng

      t.references :parent

      t.timestamps null: false
    end
  end
end
