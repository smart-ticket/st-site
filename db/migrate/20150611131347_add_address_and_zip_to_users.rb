class AddAddressAndZipToUsers < ActiveRecord::Migration
  def change
    add_column :users, :address, :string
    add_column :users, :zip, :string
  end
end
