class CreateGalleryImages < ActiveRecord::Migration
  def change
    create_table :gallery_images do |t|
      t.string :description
      t.string :image
      t.integer :event_id

      t.timestamps null: false
    end
  end
end
