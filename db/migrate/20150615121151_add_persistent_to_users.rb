class AddPersistentToUsers < ActiveRecord::Migration
  def change
    add_column :users, :persistent, :boolean
  end
end
