class CreateCommentships < ActiveRecord::Migration
  def change
    create_table :commentships do |t|
      t.integer :news_item_id
      t.integer :comment_id
    end
  end
end
