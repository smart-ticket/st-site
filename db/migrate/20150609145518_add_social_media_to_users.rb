class AddSocialMediaToUsers < ActiveRecord::Migration
  def change
    add_column :users, :profile_facebook, :string
    add_column :users, :profile_googleplus, :string
    add_column :users, :profile_twitter, :string
    add_column :users, :profile_spotify, :string
  end
end
