class AddShowRealNameToUsers < ActiveRecord::Migration
  def change
    add_column :users, :show_real_name, :boolean
  end
end
