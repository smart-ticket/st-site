class CreateAcceptMessages < ActiveRecord::Migration
  def change
    create_table :accept_messages do |t|
      t.text :content
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :accept_messages, :users
  end
end
