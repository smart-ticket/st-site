class CreateNewsItems < ActiveRecord::Migration
  def change
    create_table :news_items do |t|
      t.text :content
      t.references :user
      t.references :event

      t.timestamps null: false
    end
  end
end
