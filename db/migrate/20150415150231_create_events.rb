class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.string :subtitle
      t.date :start_date
      t.time :start_time
      t.date :end_date
      t.time :end_time
      t.text :description
      t.text :short_description

      t.boolean :group
      t.boolean :private
      t.boolean :published
      t.string :approved

      t.string :title_image

      t.string :video
      t.string :video_html

      t.references :category
      t.references :venue
      t.references :parent

      t.timestamps null: false
    end
  end
end
