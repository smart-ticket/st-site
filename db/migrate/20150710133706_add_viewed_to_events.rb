class AddViewedToEvents < ActiveRecord::Migration
  def change
    add_column :events, :viewed, :integer, default: 0
  end
end
