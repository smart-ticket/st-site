class AddUserAttributesToUser < ActiveRecord::Migration
  def change
    add_column :users, :nickname, :string
    add_column :users, :date_of_birth, :date
  end
end
