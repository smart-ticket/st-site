class CreateApprovalMessages < ActiveRecord::Migration
  def change
    create_table :approval_messages do |t|
      t.text :content
      t.references :event, index: true

      t.timestamps null: false
    end
    add_foreign_key :approval_messages, :events
  end
end
