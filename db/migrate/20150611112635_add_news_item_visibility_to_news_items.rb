class AddNewsItemVisibilityToNewsItems < ActiveRecord::Migration
  def change
    add_column :news_items, :news_item_visibility, :string
  end
end
