class AddOrganizersAndArtistsFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :short_description, :text
    add_column :users, :description, :text
    add_column :users, :web, :string
  end
end
