class AddAttachmentGalleryImageToGalleryImages < ActiveRecord::Migration
  def self.up
    change_table :gallery_images do |t|
      t.attachment :gallery_image
    end
  end

  def self.down
    remove_attachment :gallery_images, :gallery_image
  end
end
