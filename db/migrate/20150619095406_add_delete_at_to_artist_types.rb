class AddDeleteAtToArtistTypes < ActiveRecord::Migration
  def change
    add_column :artist_types, :deleted_at, :datetime
    add_index :artist_types, :deleted_at
  end
end
