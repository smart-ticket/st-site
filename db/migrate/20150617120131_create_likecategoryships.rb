class CreateLikecategoryships < ActiveRecord::Migration
  def change
    create_table :likecategoryships do |t|
      t.integer :user_id
      t.integer :category_id
    end
  end
end
