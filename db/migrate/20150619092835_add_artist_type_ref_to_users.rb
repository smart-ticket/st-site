class AddArtistTypeRefToUsers < ActiveRecord::Migration
  def change
    add_reference :users, :artist_type, index: true
    add_foreign_key :users, :artist_types
  end
end
