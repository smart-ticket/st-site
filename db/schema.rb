# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150715100841) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accept_messages", force: :cascade do |t|
    t.text     "content"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "accept_messages", ["user_id"], name: "index_accept_messages_on_user_id", using: :btree

  create_table "approval_messages", force: :cascade do |t|
    t.text     "content"
    t.integer  "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "approval_messages", ["event_id"], name: "index_approval_messages_on_event_id", using: :btree

  create_table "artist_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
  end

  add_index "artist_types", ["deleted_at"], name: "index_artist_types_on_deleted_at", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.integer  "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
  end

  add_index "categories", ["deleted_at"], name: "index_categories_on_deleted_at", using: :btree

  create_table "commentships", force: :cascade do |t|
    t.integer "news_item_id"
    t.integer "comment_id"
  end

  create_table "countries", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
  end

  add_index "countries", ["deleted_at"], name: "index_countries_on_deleted_at", using: :btree

  create_table "events", force: :cascade do |t|
    t.string   "title"
    t.string   "subtitle"
    t.date     "start_date"
    t.time     "start_time"
    t.date     "end_date"
    t.time     "end_time"
    t.text     "description"
    t.text     "short_description"
    t.boolean  "group"
    t.boolean  "private"
    t.boolean  "published"
    t.string   "approved"
    t.string   "title_image"
    t.string   "video"
    t.string   "video_html"
    t.integer  "category_id"
    t.integer  "venue_id"
    t.integer  "parent_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "user_id"
    t.boolean  "featured"
    t.integer  "viewed",            default: 0
  end

  add_index "events", ["user_id"], name: "index_events_on_user_id", using: :btree

  create_table "fanships", force: :cascade do |t|
    t.integer "user_id"
    t.integer "fan_id"
  end

  create_table "friendships", force: :cascade do |t|
    t.integer "user_id"
    t.integer "friend_id"
    t.boolean "accepted"
  end

  create_table "gallery_images", force: :cascade do |t|
    t.string   "description"
    t.string   "image"
    t.integer  "event_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "gallery_image_file_name"
    t.string   "gallery_image_content_type"
    t.integer  "gallery_image_file_size"
    t.datetime "gallery_image_updated_at"
  end

  create_table "likecategoryships", force: :cascade do |t|
    t.integer "user_id"
    t.integer "category_id"
  end

  create_table "likenewsitemships", force: :cascade do |t|
    t.integer "news_item_id"
    t.integer "user_id"
  end

  create_table "memberships", force: :cascade do |t|
    t.integer "user_id"
    t.integer "member_id"
  end

  create_table "news_items", force: :cascade do |t|
    t.text     "content"
    t.integer  "user_id"
    t.integer  "event_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "news_item_visibility"
  end

  create_table "performships", force: :cascade do |t|
    t.integer "user_id"
    t.integer "event_id"
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string  "name"
    t.integer "taggings_count", default: 0
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email"
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "surname"
    t.string   "role"
    t.string   "provider"
    t.string   "uid"
    t.datetime "deleted_at"
    t.string   "sex"
    t.string   "avatar"
    t.string   "nickname"
    t.date     "date_of_birth"
    t.integer  "country_id"
    t.string   "city"
    t.string   "profile_facebook"
    t.string   "profile_googleplus"
    t.string   "profile_twitter"
    t.string   "profile_spotify"
    t.boolean  "accepted"
    t.string   "address"
    t.string   "zip"
    t.text     "short_description"
    t.text     "description"
    t.string   "web"
    t.boolean  "persistent"
    t.integer  "artist_type_id"
    t.boolean  "show_real_name"
  end

  add_index "users", ["artist_type_id"], name: "index_users_on_artist_type_id", using: :btree
  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["country_id"], name: "index_users_on_country_id", using: :btree
  add_index "users", ["deleted_at"], name: "index_users_on_deleted_at", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

  create_table "venues", force: :cascade do |t|
    t.string   "name"
    t.string   "street"
    t.integer  "zip"
    t.string   "city"
    t.float    "lat"
    t.float    "lng"
    t.integer  "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
  end

  add_index "venues", ["deleted_at"], name: "index_venues_on_deleted_at", using: :btree

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",      null: false
    t.integer  "item_id",        null: false
    t.string   "event",          null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
    t.text     "object_changes"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

  add_foreign_key "accept_messages", "users"
  add_foreign_key "approval_messages", "events"
  add_foreign_key "events", "users"
  add_foreign_key "users", "artist_types"
  add_foreign_key "users", "countries"
end
