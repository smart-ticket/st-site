json.array!(@venues) do |venue|
  json.extract! venue, :id, :name, :street, :zip, :city, :lat, :lng, :parent
  json.url venue_url(venue, format: :json)
end
