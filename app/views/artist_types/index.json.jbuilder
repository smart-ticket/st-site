json.array!(@artist_types) do |artist_type|
  json.extract! artist_type, :id, :name
  json.url artist_type_url(artist_type, format: :json)
end
