module EventsHelper
  def format_date_time(date_time)
    I18n.l date_time.in_time_zone("CET"), format: :date_and_time
  end
end
