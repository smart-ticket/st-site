class CategorySerializer < ActiveModel::Serializer
  attributes :id, :name, :translate_name
end
