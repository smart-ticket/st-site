class ArtistTypeSerializer < ActiveModel::Serializer
  attributes :id, :name
end
