$(document).ready(function () {
  updateCharCountHint('#user_news_item_content', $('#user_news_item_content ~ .hint').html());
  if ($('#user_news_item_content').val() != undefined && $('#user_news_item_content').val() !== "") {
    $('#mdhtmlform-user-news_item_content_new').show();
  } else {
    $('#mdhtmlform-user-news_item_content_new').hide();
  }

  $('#user_news_item_content').on('input', function () {
    if ($(this).val() !== "") {
      $('#mdhtmlform-user-news_item_content_new').fadeIn("fast");
    } else {
      $('#mdhtmlform-user-news_item_content_new').fadeOut("fast");
    }
  });
});
