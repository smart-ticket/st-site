$(document).ready(function () {
  updateCharCountHint('#user_short_description', $('#user_short_description ~ .hint').html());
  if ($('#user_short_description').val() != undefined && $('#user_short_description').val() !== "") {
    $('#mdhtmlform-user-short').show();
  } else {
    $('#mdhtmlform-user-short').hide();
  }

  updateCharCountHint('#user_description', $('#user_description ~ .hint').html());
  if ($('#user_description').val() != undefined && $('#user_description').val() !== "") {
    $('#mdhtmlform-user-long').show();
  } else {
    $('#mdhtmlform-user-long').hide();
  }

  $('#user_short_description').on('input', function () {
    if ($(this).val() !== "") {
      $('#mdhtmlform-user-short').fadeIn("fast");
    } else {
      $('#mdhtmlform-user-short').fadeOut("fast");
    }
  });

  $('#user_description').on('input', function () {
    if ($(this).val() !== "") {
      $('#mdhtmlform-user-long').fadeIn("fast");
    } else {
      $('#mdhtmlform-user-long').fadeOut("fast");
    }
  });
});
