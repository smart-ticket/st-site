// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).ready(function () {
  setTimeout(function () {
    new Chartist.Line('#ct-chart-1', {
      labels: ['1', '2', '3', '4', '5', '6'],
      series: [
        {
          name: 'Fibonacci sequence',
          data: [1, 2, 3, 5, 8, 13]
        },
        {
          name: 'Golden section',
          data: [1, 1.618, 2.618, 4.236, 6.854, 11.09]
        }
      ]
    });

    var $chart = $('#ct-chart-1');

    var $toolTip = $chart
      .append('<div class="tooltip"></div>')
      .find('.tooltip')
      .hide();

    $chart.on('mouseenter', '.ct-point', function() {
      var $point = $(this),
        value = $point.attr('ct:value'),
        seriesName = $point.parent().attr('ct:series-name');
      $toolTip.html(seriesName + '<br>' + value).show();
    });

    $chart.on('mouseleave', '.ct-point', function() {
      $toolTip.hide();
    });

    $chart.on('mousemove', function(event) {
      $toolTip.css({
        left: (event.offsetX || event.originalEvent.layerX) - $toolTip.width() / 2 - 10,
        top: (event.offsetY || event.originalEvent.layerY) - $toolTip.height() - 40
      });
    });

    new Chartist.Bar('#ct-chart-2', {
      labels: [1, 2, 3, 4, 5],
      series: [[5, 2, 8, 3, 6],
              [2, 1, 6, 5, 7]]
    });

    new Chartist.Line('#ct-chart-3', {
    labels: [1, 2, 3, 4, 5, 6, 7, 8],
    series: [
      [1, 2, 3, 1, -2, 0, 1, 0],
      [-2, -1, -2, -1, -2.5, -1, -2, -1],
      [0, 0, 0, 1, 2, 2.5, 2, 1],
      [2.5, 2, 1, 0.5, 1, 0.5, -1, -2.5]
    ]
    }, {
      high: 3,
      low: -3,
      showArea: true,
      showLine: false,
      showPoint: false,
      fullWidth: true,
      axisX: {
        showLabel: false,
        showGrid: false
      }
    });

    var datai = {
      series: [5, 3, 4]
    };
    var spocitaj = function(a, b) {
      return a + b;
    };

    new Chartist.Pie('#ct-chart-4', datai, {
      labelInterpolationFnc: function(value) {
        return Math.round(value / datai.series.reduce(spocitaj) * 100) + '%';
      }
    });

}, 1);
});
