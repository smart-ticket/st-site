function removeAllSelectSubElements(nested) {
  var name = "category";
  for (var i = 0; i < 10; i++) {
    name = "sub" + name;
    if (i >= nested-1) {
      var element = $("#event_" + name + "_id");
      if (element !== null) {
        element.remove();
      }
    }
  }
}

function createSelectElement(category, nested) {
  var name = "category";
  for (var i = 0; i < nested; i++) {
    name = "sub" + name;
  }
  var element = $("#event_" + name + "_id");

  $.ajax({
    type: "GET",
    url: "/list_categories/" + category,
    dataType: "json",
    success: function(data) {
      if (element !== null) {
        removeAllSelectSubElements(nested);
      }

      if (category !== "" && data.length > 0) {
        var options = "<option value>Vyberte kategóriu</option>";
        data.forEach(function(cat) {
          options += "<option value=\"" + cat.id + "\">" + cat.translate_name + "</option>";
        });

        if (element !== null) {
          $(".js-category").append("<select onchange=\"createSelectElement(this.options[this.selectedIndex].value, " + (nested+1) + ")\" class=\"select\" name=\"event[" + name + "_id]\" id=\"event_" + name + "_id\">" + options + "</select>");
        }
      }
    },
    error: function() {
      if (element != null) {
        removeAllSelectSubElements(nested);
      }
    }

  });
}

function disableTagList() {
  var privateCheck = $("#event_private");
  var tagList = $("#event_tag_list");
  tagList.attr("disabled", privateCheck.is(":checked"));
}

$(document).ready(function () {
  updateCharCountHint('#event_title', $('#event_title ~ .hint').html());
  updateCharCountHint('#event_subtitle', $('#event_subtitle ~ .hint').html());

  updateCharCountHint('#event_short_description', $('#event_short_description ~ .hint').html());
  if ($('#event_short_description').val() != undefined && $('#event_short_description').val() !== "") {
    $('#mdhtmlform-event-short').show();
  }
  else {
    $('#mdhtmlform-event-short').hide();
  }

  updateCharCountHint('#event_description', $('#event_description ~ .hint').html());
  if ($('#event_description').val() != undefined && $('#event_description').val() !== "") {
    $('#mdhtmlform-event-long').show();
  }
  else {
    $('#mdhtmlform-event-long').hide();
  }

  $('#event_short_description').on('input', function () {
    if ($(this).val() !== "") {
      $('#mdhtmlform-event-short').fadeIn("fast");
    } else {
      $('#mdhtmlform-event-short').fadeOut("fast");
    }
  });

  $('#event_description').on('input', function () {
    if ($(this).val() !== "") {
      $('#mdhtmlform-event-long').fadeIn("fast");
    } else {
      $('#mdhtmlform-event-long').fadeOut("fast");
    }
  });

  $('input:radio[name=sortMusicBy]').click(function () {
    if ($(this).checked) { return; }

    switch($(this).val()) {
    case 'newest':
        $('#music-nearest').hide();
        $('#music-popular').hide();
        $('#music-newest').show();
        break;
    case 'nearest':
        $('#music-newest').hide();
        $('#music-popular').hide();
        $('#music-nearest').show();
        break;
    case 'popular':
        $('#music-newest').hide();
        $('#music-nearest').hide();
        $('#music-popular').show();
        break;
      }
  });

});
