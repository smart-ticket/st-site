// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require jquery_nested_form
//= require jquery.slick
//= require foundation
//= require underscore
//= require gmaps/google
//= require showdown
//= require mdhtmlform
//= require chartist/dist/chartist
//= require_tree .
//= require turbolinks

function updateCharCountHintWithSelector(element, desc, selector, css_class) {
  var chars = $(element).val() != undefined ? $(element).val().length : 0;
  $('#' + $(element).attr('id') + (css_class != null ? css_class : '') + ' ' + selector + ' .hint').html('(znakov: ' + chars + desc);
}

function updateCharCountHint(element, desc) {
  updateCharCountHintWithSelector(element, desc, '~', null);
}

function updateCharCountHintDirectWithClass(element, desc, css_class) {
  updateCharCountHintWithSelector(element, desc, '+', css_class);
}

$(function(){ $(document).foundation(); });

$(document).ready(function() {
  $.fn.extend({
    clear_form_errors: function() {
      return this.find('small.error').remove();
    },

    render_form_errors: function(model_name, errors) {
      var form;
      form = this;
      this.clear_form_errors();
      return $.each(errors, function(field, messages) {
        var input;
        input = form.find('input, select, textarea').filter(function() {
          var name;
          name = $(this).attr('name');
          if (name) {
            return name.match(new RegExp(model_name + '\\[' + field + '\\(?'));
          }
        });
        //input.closest('.form-group').addClass('has-error');
        var hint = input.parent().find('div.hint');
        var hintValue = hint.html();
        hint.remove();
        var error = input.parent().append('<small class="error" style="margin-top: -17px;">' + $.map(messages, function(m) {
          return m.charAt(0).toUpperCase() + m.slice(1);
        }).join('<br/>') + '</small>');
        if (hintValue != undefined) {
          input.parent().append('<div class="hint" style="margin-top: -12px;">' + hintValue + '</div>');
        }
        return error;
      });
    }
  });

  $("form#sign_in_user").bind("ajax:success", function(e, data, status, xhr) {
    if (data.success) {
      location.reload(true);
      //$('#popup-login').foundation('reveal', 'close');
    }
    else {
      $("form#sign_in_user").render_form_errors('user', data.errors);
    }
  });

  $("form#sign_up_user").bind("ajax:success", function(e, data, status, xhr) {
    if (data.success) {
      location.reload(true);
      //$('#popup-signup').foundation('reveal', 'close');
    }
    else {
      $("form#sign_up_user").render_form_errors('user', data.errors);
    }
  });

  $("form#password_user").bind("ajax:success", function(e, data, status, xhr) {
    if (data.success) {
      $('#popup-password-reset').foundation('reveal', 'close');
      $('#flash-notice').html("");
      $('#flash-notice').append("<div data-alert class='alert-box success' style='margin-bottom: 0;'><div>Inštrukcie boli zaslané na uvedený e-mail.</div><a href='#' class='close'>&times;</a></div>");
    }
    else {
      $("form#password_user").render_form_errors('user', data.errors);
    }
  });

  $("form#confirmation_user").bind("ajax:success", function(e, data, status, xhr) {
    if (data.success) {
      $('#popup-confirmation').foundation('reveal', 'close');
      $('#flash-notice').html("");
      $('#flash-notice').append("<div data-alert class='alert-box success' style='margin-bottom: 0;'><div>Inštrukcie boli zaslané na uvedený e-mail.</div><a href='#' class='close'>&times;</a></div>");
    }
    else {
      $("form#confirmation_user").render_form_errors('user', data.errors);
    }
  });

  $('.slider').slick({
  /*  slidesToShow: 3,
    slidesToScroll: 1, */
    autoplay: true,
    autoplaySpeed: 2500,
    arrows: false
  }); // slider
});
