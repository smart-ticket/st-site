class Fanship < ActiveRecord::Base
  belongs_to :user
  belongs_to :fan, class_name: "User"
end
