class User < ActiveRecord::Base
  include ClassyEnum::ActiveRecord

  scope :users, -> { where(role: :user) }
  scope :organizers, -> { where(role: :organizer) }
  scope :artists, -> { where(role: :artist) }
  scope :admins, -> { where(role: :admin) }

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :lockable, :validatable, :confirmable, :omniauthable, omniauth_providers: [:facebook, :google_oauth2]

  attr_accessor :login


  # Common fields
  validates :email, email: true

  classy_enum_attr :role, default: "user"

  mount_uploader :avatar, AvatarUploader

  belongs_to :country

  has_many :news_items, dependent: :destroy
  accepts_nested_attributes_for :news_items, allow_destroy: true

  acts_as_paranoid


  # User fields
  validates :date_of_birth, date: { before_or_equal_to: Proc.new { Date.today }, message: I18n.t(:'errors.messages.user.date_of_birth') }, unless: "date_of_birth.nil?"

  classy_enum_attr :sex, allow_blank: true, default: ""

  has_many :friendships
  has_many :inverse_friendships, class_name: "Friendship", foreign_key: "friend_id"

  has_many :active_friends, -> { where(friendships: { accepted: true}) }, :through => :friendships, :source => :friend
  has_many :inverse_friends, -> { where(friendships: { accepted: true}) }, :through => :inverse_friendships, :source => :user
  has_many :pending_friends, -> { where(friendships: { accepted: false}) }, :through => :friendships, :source => :friend
  has_many :requested_friends, -> { where(friendships: { accepted: false}) }, :through => :inverse_friendships, :source => :user

  has_many :inverse_fanships, class_name: "Fanship", foreign_key: "fan_id"
  has_many :inverse_fans, :through => :inverse_fanships, :source => :user


  # Organizer fields
  has_many :events


  # Artist fields
  validates :name, presence: true, length: { minimum: 2, maximum: 80 }, if: "is_artist?"

  validates :short_description, length: { maximum: 500 }, if: "is_artist?"
  validates :description, length: { maximum: 3000 }, if: "is_artist?"

  has_many :memberships
  has_many :inverse_memberships, class_name: "Membership", foreign_key: "member_id"

  has_many :members, :through => :memberships, :source => :member
  has_many :inverse_members, :through => :inverse_memberships, :source => :user

  accepts_nested_attributes_for :memberships

  has_many :performships
  has_many :perform_events, :through => :performships, :source => :event

  belongs_to :artist_type


  # User and organizer fields
  validates :nickname, presence: true, uniqueness: { case_sensitive: false }, if: "is_user? || is_organizer?"


  # User and artist fields
  has_many :likecategoryships
  has_many :likecategories, :through => :likecategoryships, :source => :category
  accepts_nested_attributes_for :likecategoryships


  # Organizer and artist fields
  has_many :fanships
  has_many :fans, :through => :fanships, :source => :fan

  has_one :accept_message, dependent: :destroy
  accepts_nested_attributes_for :accept_message, allow_destroy: true


  def login=(login)
    @login = login
  end

  def login
    @login || self.nickname || self.email
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_hash).where(["lower(nickname) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      conditions[:email].downcase! if conditions[:email]
      where(conditions.to_hash).first
    end
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

  def self.facebook_from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.name = auth.extra.raw_info.first_name
      user.surname = auth.extra.raw_info.last_name
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.uid = auth.uid
      user.provider = auth.provider
    end
  end

  def self.google_from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.name = auth.info.first_name
      user.surname = auth.info.last_name
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.uid = auth.uid
      user.provider = auth.provider
    end
  end

  def age
    if date_of_birth != nil
      now = Time.now.to_date
      now.year - date_of_birth.year - (date_of_birth.to_date.change(:year => now.year) > now ? 1 : 0)
    end
  end

  def is_correct_real_name?
    (name != nil && name != "") && (surname != nil && surname != "")
  end

  def get_show_name
    if nickname != nil && nickname != ""
      nickname
    else
      if name != nil && name != ""
        name
      else
        email
      end
    end
  end

  def get_name
    if show_real_name && is_correct_real_name?
      name.to_s + " " + surname.to_s
    else
      get_show_name
    end
  end

  def get_sort_name
    if show_real_name && is_correct_real_name?
      surname.to_s
    else
      get_show_name
    end
  end

  def is_parent?(searched_parent)
    if inverse_members == nil || inverse_members == [] || searched_parent == nil
      false
    elsif inverse_members.select { |m| m.id == searched_parent.id } != nil && inverse_members.select { |m| m.id == searched_parent.id } != []
      true
    else
      inverse_members.each do |m|
        return true if m.is_parent?(searched_parent)
      end
      false
    end
  end

  def is_email_empty?
    return email == ""
  end

  def markdown_short_description
    if short_description != nil
      Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true, tables: true).render(short_description)
    end
  end

  def markdown_description
    if short_description != nil
      Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true, tables: true).render(description)
    end
  end

  def friends
    active_friends | inverse_friends
  end

  def likeorganizers
    inverse_fans.select{ |u| u.is_organizer? }
  end

  def likeartists
    inverse_fans.select{ |u| u.is_artist? }
  end

  Role.all.each do |r|
    define_method "is_#{r.to_s}?" do
      role == :"#{r.to_s}"
    end
  end

end
