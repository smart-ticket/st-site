class NewsItem < ActiveRecord::Base
  include ClassyEnum::ActiveRecord

  scope :ordered, -> { order(created_at: :desc) }

  belongs_to :user
  belongs_to :event

  validates :content, presence: true, length: { maximum: 1000 }

  classy_enum_attr :news_item_visibility, default: "public"

  has_many :commentships
  has_many :comments, :through => :commentships, :source => :comment, :dependent => :destroy

  has_many :inverse_commentships, class_name: "Commentship", foreign_key: "comment_id"
  has_many :parents, :through => :inverse_commentships, :source => :news_item

  has_many :likenewsitemships
  has_many :likenewsitems, :through => :likenewsitemships, :source => :user

  def markdown_content
    if content != nil
      Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true, tables: true).render(content)
    end
  end

  def recursive_destroy_comments
    comments.each do |comment|
      comment.recursive_destroy_comments
      comment.destroy
    end
  end
end
