class Category < ActiveRecord::Base
  validates :name, presence: true

  has_many :subcategories, class_name: "Category", foreign_key: "parent_id", dependent: :destroy
  belongs_to :parent, class_name: "Category"

  has_many :likecategoryships
  has_many :likeusers, :through => :likecategoryships, :source => :user

  has_many :events

  acts_as_paranoid

  def parent_categories
    categories = []
    cat = self
    while cat != nil do
      categories.append(cat)
      cat = cat.parent
    end
    categories.reverse
  end

  def is_category?(category)
    if category == nil || name == category.name
      true
    elsif parent_id != nil
      Category.with_deleted.find(parent_id).is_category?(category)
    else
      false
    end
  end

  def is_parent?(searched_parent)
    if parent_id == nil || searched_parent == nil
      false
    elsif parent_id == searched_parent
      true
    else
      Category.with_deleted.find(parent_id).is_parent?(searched_parent)
    end
  end

  def split_name
    if parent_id != nil
      Category.with_deleted.find(parent_id).split_name().append(name)
    else
      [name]
    end
  end

  def to_s
    if parent_id != nil
      Category.with_deleted.find(parent_id).to_s + " / " + translate_name
    else
      translate_name
    end
  end

  def translate_name
    I18n.t("category." + name)
  end

  def self.translate_name(name)
    I18n.t("category." + name)
  end
end
