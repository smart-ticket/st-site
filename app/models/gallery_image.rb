class GalleryImage < ActiveRecord::Base
  scope :ordered, -> { order(id: :asc) }

  belongs_to :event

  has_attached_file :gallery_image,
    :path => ":rails_root/public/gallery_images/:style/:id/:filename",
    :url  => "/gallery_images/:style/:id/:filename",
    :default_style => :normal,
    :styles => {
      :normal => "1000x1000>",
      :thumb => "100x100" }

  do_not_validate_attachment_file_type :gallery_image

  def format_description
    if !description
      ""
    else
      description
    end
  end
end
