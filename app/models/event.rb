class Event < ActiveRecord::Base
  include ClassyEnum::ActiveRecord

  has_paper_trail

  scope :ordered, -> { order(title: :asc) }

  classy_enum_attr :approved, default: "editing"

  validates :title, presence: true, length: { minimum: 5, maximum: 80 }, unless: :has_parent?
  validates :title, uniqueness: { case_sensitive: false, message: I18n.t(:'errors.messages.event.title'), conditions: -> { where(parent: nil) } }, unless: :has_parent?

  validates :subtitle, length: { maximum: 100 }

  validates :short_description, :description, presence: true, unless: :has_parent?

  validates :start_date, :start_time, presence: true, if: "!group?"

  validates :start_date, date: { after_or_equal_to: Proc.new { Date.today }, message: I18n.t(:'errors.messages.event.date') }, unless: "start_date.nil?"
  validates :start_date, date: { before_or_equal_to: :end_date, message: I18n.t(:'errors.messages.event.start_date') }, unless: "end_date.nil?"

  validates :end_date, date: { after_or_equal_to: Proc.new { Date.today }, message: I18n.t(:'errors.messages.event.date') }, unless: "end_date.nil?"
  validates :end_date, date: { after_or_equal_to: :start_date, message: I18n.t(:'errors.messages.event.end_date') }, unless: "end_date.nil?"

  validates :short_description, length: { minimum: 80, maximum: 500 }, unless: :has_parent?
  validates :description, length: { minimum: 200, maximum: 3000 }, unless: :has_parent?

  belongs_to :category
  validates :category, presence: true, unless: :has_parent?

  belongs_to :venue
  validates :venue, presence: true, if: "!group? && (!has_parent? || (has_parent? && parent.get_venue_validation.nil?))"

  belongs_to :user

  mount_uploader :title_image, TitleImageUploader
  validates :title_image, presence: true, unless: :has_parent?

  has_many :subevents, class_name: "Event", foreign_key: "parent_id", dependent: :destroy
  belongs_to :parent, class_name: "Event"

  has_many :gallery_images, dependent: :destroy
  accepts_nested_attributes_for :gallery_images, allow_destroy: true

  has_many :performships
  has_many :performers, :through => :performships, :source => :user
  accepts_nested_attributes_for :performships

  has_one :approval_message, dependent: :destroy
  accepts_nested_attributes_for :approval_message, allow_destroy: true

  has_many :comments, class_name: "NewsItem", foreign_key: "event_id", dependent: :destroy
  accepts_nested_attributes_for :comments, allow_destroy: true

  auto_html_for :video do
    youtube(width: 350, height: 250, autoplay: false)
    vimeo(width: 350, height: 250, autoplay: false)
  end

  acts_as_taggable

  def has_tag?(tag)
    tag_list.include?(tag)
  end

  def has_parent?
    if parent == nil
      return false
    else
      return true
    end
  end

  def is_base_event?
    !has_parent?
  end

  def get_title
    if title != nil && title != ""
      title
    else
      parent.get_title
    end
  end

  def get_full_title
    if parent != nil
      if title != nil && title != ""
        parent.get_full_title + " / " + title
      else
        parent.get_full_title
      end
    else
      title
    end
  end

  def format_title
    if title != nil && title != ""
      title
    elsif start_date != nil && start_time != nil
      format_start_date_time
    else
      t = get_start_datetime
      if t != nil
        I18n.l t, format: :date_and_time_long
      else
        I18n.t(:"errors.unknown")
      end
    end
  end

  def get_subtitle
    if subtitle != nil && subtitle != ""
      subtitle
    else
      parent.get_subtitle
    end
  end

  def get_category
    if category_id != nil
      Category.with_deleted.find(category_id)
    else
      parent.get_category
    end
  end

  def get_venue_validation
    if venue != nil
      venue
    elsif parent != nil
      parent.get_venue_validation
    else
      nil
    end
  end

  def get_venue
    if venue_id != nil
      Venue.with_deleted.find(venue_id)
    elsif parent != nil
      parent.get_venue
    else
      nil
    end
  end

  def get_title_image
    if title_image.present?
      title_image
    elsif parent != nil
      parent.get_title_image
    else
      nil
    end
  end

  def get_start_datetime
    if start_date != nil && start_time != nil
      DateTime.parse(format_start_date_time_uni)
    elsif subevents != nil && subevents.count > 0
      subevents.sort_by { |ev| ev.get_start_datetime }.first.get_start_datetime
    else
      nil
    end
  end

  def markdown_short_description
    if short_description != nil
      Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true, tables: true).render(short_description)
    end
  end

  def markdown_description
    if description != nil
      Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true, tables: true).render(description)
    end
  end

  def format_start_date_time
    if start_date != nil && start_time != nil
      (I18n.l start_date, format: :date_only).concat(", ").concat(I18n.l start_time, format: :time_only)
    else
      ""
    end
  end

  def format_start_date_time_uni
    if start_date != nil && start_time != nil
      (I18n.l start_date, format: :date_only_uni).concat(" ").concat(I18n.l start_time, format: :time_only_uni)
    else
      ""
    end
  end

  def format_end_date_time
    formatted = ""
    if end_date != nil
      formatted.concat((I18n.l end_date, format: :date_only))
    end
    if end_time != nil
      formatted.concat(", ").concat(I18n.l end_time, format: :time_only)
    end
    formatted
  end

  def belongs_to_user(u)
    return (u == user && (parent == nil || parent.belongs_to_user(u))) || u.is_admin?
  end

  def is_visible?
    if parent == nil
      !private && published && approved == :is_approved && get_start_datetime != nil
    else
      parent.is_visible?
    end
  end

end
