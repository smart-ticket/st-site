class ArtistType < ActiveRecord::Base
  has_many :users

  acts_as_paranoid

  def have_sex?
    self.class.have_sex?(name)
  end

  def translate_name(sex)
    self.class.translate_name(name, sex)
  end

  def translate_name_completely
    self.class.translate_name_completely(name)
  end

  def self.have_sex?(name)
    translated_male = I18n.t("artist_type." + name + ".male", default: "not_exist")
    translated_female = I18n.t("artist_type." + name + ".female", default: "not_exist")

    if translated_male != "not_exist" && translated_female != "not_exist"
      true
    else
      false
    end
  end

  def self.translate_name(name, sex)
    if have_sex?(name)
      if sex != nil && sex != ""
        I18n.t("artist_type." + name + "." + sex)
      else
        translate_name_completely(name)
      end
    else
      I18n.t("artist_type." + name)
    end
  end

  def self.translate_name_completely(name)
    if have_sex?(name)
      translate_name(name, "male") + " / " + translate_name(name, "female")
    else
      translate_name(name, nil)
    end
  end

end
