class Venue < ActiveRecord::Base
  validates :name, presence: true

  has_many :subvenues, class_name: "Venue", foreign_key: "parent_id", dependent: :destroy
  belongs_to :parent, class_name: "Venue"

  has_many :events

  acts_as_paranoid

  def get_street
    if street != nil
      street
    elsif parent != nil
      parent.get_street
    end
  end

  def get_zip
    if zip != nil
      zip
    elsif parent != nil
      parent.get_zip
    end
  end

  def get_city
    if city != nil
      city
    elsif parent != nil
      parent.get_city
    end
  end

  def get_lat
    if lat != nil
      lat
    elsif parent != nil
      parent.get_lat
    end
  end

  def get_lng
    if lng != nil
      lng
    elsif parent != nil
      parent.get_lng
    end
  end

  def is_parent?(searched_parent)
    if parent == nil || searched_parent == nil
      false
    elsif parent == searched_parent
      true
    else
      parent.is_parent?(searched_parent)
    end
  end

  def format_name
    result = to_s
    result += ", " + get_street if get_street != nil
    result += ", " + get_zip.to_s if get_zip != nil
    result += ", " + get_city if get_city != nil
  end

  def split_name
    if parent_id != nil
      Venue.with_deleted.find(parent_id).split_name().append(name)
    else
      [name]
    end
  end

  def to_s
    if parent_id != nil
      Venue.with_deleted.find(parent_id).to_s + " - " + name
    else
      name
    end
  end
end
