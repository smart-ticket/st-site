class Country < ActiveRecord::Base
  has_many :users

  acts_as_paranoid

  def translate_name
    I18n.t(:"#{name}", :scope => :countries)
  end
end
