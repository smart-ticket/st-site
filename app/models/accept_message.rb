class AcceptMessage < ActiveRecord::Base
  belongs_to :user

  validates :content, presence: true, length: { maximum: 500 }

  def markdown_content
    if content != nil
      Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true, tables: true).render(content)
    end
  end
end
