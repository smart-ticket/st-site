class Commentship < ActiveRecord::Base
  belongs_to :news_item
  belongs_to :comment, class_name: "NewsItem"
end
