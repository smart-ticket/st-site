class FriendshipsController < ApplicationController
  layout "social"

  # POST /friendships
  # POST /friendships.json
  def create
    user = User.find(params[:friend_id])
    if user == nil || user.role != :user || current_user.role != :user
      flash[:error] = "Tohto používateľa nemôžete požiadať o priateľstvo."
      redirect_to :back
      return
    end

    contains = false

    current_user.friendships.each do |friendship|
      if friendship.friend.id.to_s == params[:friend_id]
        contains = true
        break
      end
    end

    @friendship = current_user.friendships.build(friend_id: params[:friend_id], accepted: "false")

    if !contains && @friendship.save
      flash[:notice] = "Žiadosť o priateľstvo bola odoslaná."
      redirect_to :back
    else
      flash[:error] = "Nepodarilo sa pridať priateľa."
      redirect_to :back
    end
  end

  # PATCH/PUT /friendships/1
  # PATCH/PUT /friendships/1.json
  def update
    @friendship = Friendship.where(friend_id: current_user, user_id: params[:id]).first
    @friendship.update(accepted: true)
    if @friendship.save
      redirect_to social_path, notice: "Priateľstvo bolo potvrdené."
    else
      redirect_to social_path, notice: "Priateľstvo sa nepodarilo potvrdiť."
    end
  end

  # DELETE /friendships/1
  # DELETE /friendships/1.json
  def destroy
    @friendship = Friendship.where(friend_id: [current_user, params[:id]]).where(user_id: [current_user, params[:id]]).last
    @friendship.destroy
    flash[:notice] = "Používateľ bol odstránený zo zoznamu priateľov."
    redirect_to :back
  end

end
