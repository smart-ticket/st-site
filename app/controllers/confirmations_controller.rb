class ConfirmationsController < Devise::ConfirmationsController
  layout "social"

  def create
    resource = resource_class.send_confirmation_instructions(resource_params)

    if successfully_sent?(resource)
      return render json: { success: true }
    else
      return render json: { success: false, errors: resource.errors }
    end
  end

end
