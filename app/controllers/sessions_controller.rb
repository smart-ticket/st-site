class SessionsController < Devise::SessionsController
  layout "social"

  skip_before_filter :verify_signed_out_user

  def create
    resource = warden.authenticate!(scope: resource_name, recall: "#{controller_path}#failure")
    sign_in(resource_name, resource)
    return render json: { success: true }
  end

  def failure
    errors = []
    flash.each do |name, msg|
      errors.append(msg.to_s)
    end
    if errors.length == 0
      errors = [I18n.t(:"devise.failure.not_found_in_database")]
    end
    return render json: { success: false, errors: { "login": errors } }
  end

end
