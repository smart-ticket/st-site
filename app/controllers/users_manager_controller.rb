class UsersManagerController < ApplicationController
  before_action :find_user, only: [ :edit, :update, :show, :destroy ]

  def index
    authorize :admin_only
    @users = User.with_deleted.users.select { |u| u.email != nil }.sort { |a,b| a.get_sort_name.downcase <=> b.get_sort_name.downcase }
    @organizers = User.with_deleted.organizers.sort { |a,b| a.get_sort_name.downcase <=> b.get_sort_name.downcase }
    @artists = User.with_deleted.artists.sort { |a,b| a.get_sort_name.downcase <=> b.get_sort_name.downcase }
    if current_user.persistent
      @admins = User.with_deleted.admins.select { |u| u != current_user }.select { |u| !u.persistent? }.sort { |a,b| a.get_sort_name.downcase <=> b.get_sort_name.downcase }
    end

    render "users/index"
  end

  def new
    authorize :admin_only

    if params[:role] == "user"
      render template: "devise/registrations/new_user_by_admin"
    elsif params[:role] == "organizer"
      render template: "devise/registrations/new_organizer_by_admin"
    elsif params[:role] == "artist"
      render template: "devise/registrations/new_artist_by_admin"
    elsif params[:role] == "admin"
      render template: "devise/registrations/new_admin_by_admin"
    end
  end

  def create
    authorize :admin_only

  end

  def edit
    authorize @user

    render template: "devise/registrations/edit", locals: { resource: @user, resource_name: "user" }
  end

  def update
    authorize @user

  end

  def update_accept_message
    @user = User.find(params[:users_manager_id])

    authorize @user, :update_accept_message?

    respond_to do |format|
      if (params[:accept] || params[:not_accept])
        accept_message = AcceptMessage.create(user: @user, content: params[:user][:accept_message_attributes][:content])

        if params[:accept]
          notice_text = I18n.t(:"success.messages.users.accepted")
          @user.accepted = true
        else
          notice_text = I18n.t(:"success.messages.users.not_accepted")
          @user.accepted = false
        end

        if accept_message != nil && accept_message.save && @user.save
          format.html { redirect_to users_manager_index_path, notice: notice_text }
          format.json { render :show, status: :updated, location: @user }
        else
          format.html { redirect_to users_manager_index_path, alert: I18n.t(:"errors.messages.user.blank_accept_message") }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  def show
    authorize @user

    if @user.is_user?
      @likecategories = @user.likecategories.sort { |a,b| a.translate_name.downcase <=> b.translate_name.downcase }
      @likeorganizers = @user.likeorganizers.sort { |a,b| a.get_name.downcase <=> b.get_name.downcase }
      @likeartists = @user.likeartists.sort { |a,b| a.get_name.downcase <=> b.get_name.downcase }
      @friends = @user.friends.sort { |a,b| a.get_sort_name.downcase <=> b.get_sort_name.downcase }
    elsif @user.is_organizer?
      @fans = @user.fans.sort { |a,b| a.get_sort_name.downcase <=> b.get_sort_name.downcase }
      @events = @user.events.select { |e| e.is_base_event? }.sort { |a,b| a.get_title.downcase <=> b.get_title.downcase }
    elsif @user.is_artist?
      @fans = @user.fans.sort { |a,b| a.get_sort_name.downcase <=> b.get_sort_name.downcase }
      @members = @user.members.sort { |a,b| a.get_name.downcase <=> b.get_name.downcase }
      @inverse_members = @user.inverse_members.sort { |a,b| a.get_name.downcase <=> b.get_name.downcase }
      @perform_events = @user.perform_events.select { |e| e.is_base_event? }.sort { |a,b| a.get_title.downcase <=> b.get_title.downcase }
    end

    render "users/show"
  end

  def destroy
    authorize @user

    if !@user.paranoia_destroyed? || params[:erase] == "true"
      if @user.paranoia_destroyed?
        @user.deleted_at = nil
        @user.save!
      end
      RegistrationsController.new.destroy_user(@user, params[:erase], false)
      respond_to do |format|
        if !@user.persistent
          format.html { redirect_to users_manager_index_path, notice: I18n.t(:"success.messages.users.destroy") }
        else
          format.html { redirect_to users_manager_index_path, alert: I18n.t(:"devise.registrations.not_destroyed") }
        end
        format.json { head :no_content }
      end
    else
      User.restore(@user.id, recursive: true)
      respond_to do |format|
        format.html { redirect_to users_manager_index_path, notice: I18n.t(:"success.messages.users.restore") }
        format.json { head :no_content }
      end
    end
  end

  private

    def find_user
      @user = User.with_deleted.find(params[:id])
    end

end
