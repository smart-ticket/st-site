class GalleryImagesController < ApplicationController
  def edit
    @event = Event.find(params[:event_id])
    @gallery_image = GalleryImage.find(params[:id])

    authorize @gallery_image
  end

  def update
    @event = Event.find(params[:event_id])
    @gallery_image = @event.gallery_images.find(params[:id])

    authorize @gallery_image

    respond_to do |format|
      if @gallery_image.update_attributes(gallery_image_params)
        format.html { redirect_to event_path(@event), notice: I18n.t(:"success.messages.gallery_images.update") }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @gallery_image.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @gallery_image = GalleryImage.find(params[:id])

    authorize @gallery_image

    @gallery_image.destroy

    respond_to do |format|
      format.html { redirect_to root_path }
      format.js
    end
  end

  private

    def gallery_image_params
      params.require(:gallery_image).permit(:description, :event_id, :gallery_image)
    end
end
