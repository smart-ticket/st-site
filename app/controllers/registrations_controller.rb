class RegistrationsController < Devise::RegistrationsController
  prepend_before_filter :require_no_authentication, only: [:new, :cancel]

  layout "social"

  def create
    if !user_signed_in? || !current_user.is_admin?
      build_resource(sign_up_params)

      if sign_up_params[:role] == "organizer"
        resource.role = :organizer
      else
        resource.role = :user
      end

      resource.save

      if resource.persisted?
        if resource.active_for_authentication?
          set_flash_message :notice, :signed_up if is_flashing_format?
          sign_up(resource_name, resource)
          return render json: { success: true }
        else
          set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_flashing_format?
          expire_data_after_sign_in!
          return render json: { success: true }
        end
      else
        clean_up_passwords resource
        return render json: { success: false, errors: resource.errors }
      end
    else
      resource = User.new(sign_up_params)
      resource.role = :user

      if sign_up_params[:role] == "organizer"
        resource.role = :organizer
      elsif sign_up_params[:role] == "artist"
        resource.role = :artist
      elsif sign_up_params[:role] == "admin"
        resource.role = :admin
      end

      if sign_up_params[:role] == "admin" && !current_user.persistent
        redirect_to users_manager_index_path, flash: { alert: I18n.t("pundit.not_authorized") }
        return
      end

      resource.skip_confirmation!
      resource.save

      redirect_to users_manager_index_path, flash: { notice: I18n.t("success.messages.users.create") }
    end
  end

  def update
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)

    if current_user.is_admin?
      if params[:user][:uid] != nil && params[:user][:uid] != current_user.id.to_s
        self.resource = User.with_deleted.find(params[:user][:uid])
      end
    end

    update_user(resource)
  end

  def destroy
    destroy_user(resource, params[:erase], true)
  end

  def update_user(resource)
    #self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

    params = account_update_params

    if devise_mapping.confirmable? && (resource.pending_reconfirmation? || !resource.confirmed?)
      params.delete :email
    end

    params[:profile_facebook] = nil if params[:profile_facebook] == "" || params[:profile_facebook] == ENV['FACEBOOK_PROFILE_BASE_URL']
    params[:profile_googleplus] = nil if params[:profile_googleplus] == "" || params[:profile_googleplus] == ENV['GOOGLE_PLUS_PROFILE_BASE_URL']
    params[:profile_twitter] = nil if params[:profile_twitter] == "" || params[:profile_twitter] == ENV['TWITTER_PROFILE_BASE_URL']
    params[:profile_spotify] = nil if params[:profile_spotify] == "" || params[:profile_spotify] == ENV['SPOTIFY_PROFILE_BASE_URL']

    if params[:memberships_attributes] && resource.is_artist?
      resource.memberships.clear

      params[:memberships_attributes].values.each do |membership|
        if membership[:_destroy] == "1"
          member = Membership.where(id: membership[:id]).last
          if member != nil
            member.destroy
          end
        elsif membership[:_destroy] == "false"
          #member = resource.memberships.find { |m| m.member_id.to_s == membership[:member_id] }
          #if member == nil
            resource.memberships.build(member_id: membership[:member_id])
          #end
        end
      end
    end

    params.delete :memberships_attributes

    if params[:likecategoryships_attributes] && (resource.is_user? || resource.is_artist?)
      resource.likecategoryships.clear

      params[:likecategoryships_attributes].values.each do |likecategoryship|
        if likecategoryship[:_destroy] == "1"
          c = Likecategoryship.where(id: likecategoryship[:id]).last
          if c != nil
            c.destroy
          end
        elsif likecategoryship[:_destroy] == "false"
          #c = resource.likecategoryships.find { |c| c.category_id.to_s == likecategoryship[:category_id] }
          #if c == nil
            resource.likecategoryships.build(category_id: likecategoryship[:category_id])
          #end
        end
      end
    end

    params.delete :likecategoryships_attributes

    if !current_user.is_admin? || resource.id == current_user.id
      resource_updated = update_resource(resource, params)
    else
      if (params[:password] != nil && params[:password] != "")
        if params[:password] == params[:password_confirmation]
          password_updated = resource.update_attributes(password: params[:password])
        else
          password_updated = false
        end

        resource_updated = resource.update_without_password(params) && password_updated

        if password_updated == false
          resource.errors.add(:password, I18n.t("errors.messages.confirmation"))
        end
      else
        resource_updated = resource.update_without_password(params)
      end
    end

    if resource.is_organizer? && resource.accepted == true
      resource.accepted = false
      resource.accept_message = nil
      resource.save
    end

    if !resource.is_correct_real_name?
      resource.show_real_name = false
      resource.save
    end

    yield resource if block_given?

    if resource_updated
      if is_flashing_format?
        flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ? :update_needs_confirmation : :updated
        set_flash_message :notice, flash_key
      end

      if !current_user.is_admin? || resource.id == current_user.id
        sign_in resource_name, resource, bypass: true
        redirect_to edit_user_registration_path(self.resource)
      else
        redirect_to edit_users_manager_path(resource)
      end

      #respond_with resource, location: after_update_path_for(resource)
    else
      clean_up_passwords resource

      if !current_user.is_admin? || resource.id == current_user.id
        respond_with resource
      else
        respond_with(resource.errors, status: :unprocessable_entity) do |format|
          format.html { render layout: "admin", template: "devise/registrations/edit" }
        end
      end
    end
  end

  def destroy_user(resource, erase, redirect)
    if resource.persistent
      #redirect_to users_manager_index_path, error: I18n.t("devise.registrations.not_destroyed")
    else
      if erase == "true" && resource.is_user?
        resource.remove_avatar!

        resource.email = nil
        resource.name = nil
        resource.surname = nil
        resource.nickname = nil
        resource.date_of_birth = nil
        resource.country = nil
        resource.address = nil
        resource.zip = nil
        resource.city = nil
        resource.sex = nil
        resource.profile_facebook = nil
        resource.profile_googleplus = nil
        resource.profile_twitter = nil
        resource.profile_spotify = nil
        resource.avatar = nil
        resource.short_description = nil
        resource.description = nil
        resource.web = nil
        resource.artist_type = nil

        [resource.friendships, resource.inverse_friendships,
         resource.fanships, resource.inverse_fanships,
         resource.memberships, resource.inverse_memberships,
         resource.performships, resource.fanships].each do |ships|
          ships.each do |ship|
            ship.destroy
          end
          ships.clear
        end

        resource.save!(validate: false)
      end

      resource.destroy
      if redirect
        Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
        set_flash_message :notice, :destroyed
        yield resource if block_given?
        respond_with_navigational(resource) { redirect_to after_sign_out_path_for(resource_name) }
      end
    end
  end

  def remove_avatar
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    self.resource.remove_avatar!
    self.resource.save!

    redirect_to edit_user_registration_path(self.resource)
  end

  def create_news_item
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    self.resource.news_items.build(content: params[:user][:news_item][:content], news_item_visibility: params[:user][:news_item][:news_item_visibility])
    self.resource.save!

    redirect_to social_path(self.resource)
  end

  def update_news_item
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)

    @news_item = NewsItem.where(id: params[:user][:news_item][:id], user_id: self.resource).first
    @news_item.update(content: params[:user][:news_item][:content], news_item_visibility: params[:user][:news_item][:news_item_visibility])

    redirect_to social_path(self.resource)
  end

  def destroy_news_item
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)

    @news_item = NewsItem.where(id: params[:id], user_id: self.resource).first
    @news_item.recursive_destroy_comments
    @news_item.destroy

    redirect_to :back, flash: { notice: I18n.t("success.messages.users.news_item.destroy") }
  end

end
