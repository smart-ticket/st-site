class FanshipsController < ApplicationController
  layout "social"

  def create
    user = User.find(params[:user_id])
    if user == nil || (!user.is_organizer? && !user.is_artist?) || !current_user.is_user?
      flash[:error] = "Tohto používateľa nemôžete pridať medzi obľúbených."
      redirect_to :back
      return
    end

    contains = false

    current_user.inverse_fanships.each do |inverse_fan|
      if inverse_fan.user.id.to_s == params[:user_id]
        contains = true
        break
      end
    end

    inverse_fan = current_user.inverse_fanships.build(user_id: params[:user_id])

    if !contains && inverse_fan.save
      flash[:notice] = "Úspešne pridaný do obľúbených."
      redirect_to :back
    else
      flash[:error] = "Nepodarilo sa pridať do obľúbených."
      redirect_to :back
    end
  end

  def destroy
    inverse_fan = Fanship.where(user_id: [current_user, params[:id]]).where(fan_id: [current_user, params[:id]]).last
    inverse_fan.destroy
    flash[:notice] = "Úspešne odstránený z obľúbených."
    redirect_to :back
  end

end
