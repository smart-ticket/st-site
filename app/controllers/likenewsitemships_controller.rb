class LikenewsitemshipsController < ApplicationController
  layout "social"

  def create
    news_item = NewsItem.find(params[:news_item_id])

    contains = false

    if news_item.user != current_user
      news_item.likenewsitemships.each do |likenewsitem|
        if likenewsitem.user.id == current_user.id
          contains = true
          break
        end
      end
    else
      contains = true
    end

    like = news_item.likenewsitemships.build(user_id: current_user.id)

    if !contains && like.save
      redirect_to :back
    else
      redirect_to :back
    end
  end

  def destroy
    like = Likenewsitemship.where(news_item_id: params[:news_item_id], user: current_user).last
    like.destroy
    redirect_to :back
  end
end
