class CountriesController < ApplicationController
  before_action :set_country, only: [:show, :edit, :update, :destroy]

  before_action :authenticate_user!, except: [:index, :show]

  # GET /countries
  # GET /countries.json
  def index
    if user_signed_in? && current_user.is_admin?
      @countries = Country.with_deleted
    else
      @countries = Country.all
    end

    @countries = @countries.sort { |a,b| ActiveSupport::Inflector.transliterate(a.name.downcase) <=> ActiveSupport::Inflector.transliterate(b.name.downcase) }
  end

  # GET /countries/1
  # GET /countries/1.json
  def show
  end

  # GET /countries/new
  def new
    @country = Country.new
    authorize :admin
  end

  # GET /countries/1/edit
  def edit
    authorize :admin
  end

  # POST /countries
  # POST /countries.json
  def create
    @country = Country.new(country_params)
    authorize :admin

    respond_to do |format|
      if @country.save
        format.html { redirect_to @country, notice: I18n.t(:"success.messages.country.create") }
        format.json { render :show, status: :created, location: @country }
      else
        format.html { render :new }
        format.json { render json: @country.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /countries/1
  # PATCH/PUT /countries/1.json
  def update
    authorize :admin

    respond_to do |format|
      if @country.update(country_params)
        format.html { redirect_to @country, notice: I18n.t(:"success.messages.country.update") }
        format.json { render :show, status: :ok, location: @country }
      else
        format.html { render :edit }
        format.json { render json: @country.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /countries/1
  # DELETE /countries/1.json
  def destroy
    authorize :admin

    if !@country.paranoia_destroyed?
      @country.destroy
      respond_to do |format|
        format.html { redirect_to countries_url, notice: I18n.t(:"success.messages.country.destroy") }
        format.json { head :no_content }
      end
    else
      Country.restore(@country.id, recursive: true)
      respond_to do |format|
        format.html { redirect_to countries_url, notice: I18n.t(:"success.messages.country.restore") }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_country
      @country = Country.with_deleted.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def country_params
      params.require(:country).permit(:name)
    end
end
