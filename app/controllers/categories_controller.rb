class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :edit, :update, :destroy]

  before_action :authenticate_user!, except: [:index, :show]

  # GET /categories
  # GET /categories.json
  def index
    respond_to do |format|
      if user_signed_in? && current_user.is_admin?
        @categories = Category.with_deleted
      else
        @categories = Category.all
      end

      format.html { @categories = @categories.sort { |a,b| a.to_s.downcase <=> b.to_s.downcase } }
      format.json do
        if params[:parent_category]
          category = nil
          if params[:parent_category] =~ /\A[-+]?[0-9]*\.?[0-9]+\Z/
            category = Category.find_by_id(params[:parent_category])
          else
            category = Category.find_by_name(params[:parent_category])
          end
          if category != nil
            render json: category.subcategories.sort { |a,b| a.translate_name.downcase <=> b.translate_name.downcase }
          end
        else
          categories = Category.where(parent: nil)
          if categories != nil
            render json: categories.sort { |a,b| a.translate_name.downcase <=> b.translate_name.downcase }
          end
        end
      end
    end
  end

  # GET /categories/1
  # GET /categories/1.json
  def show
    if user_signed_in? && current_user.is_admin?
      @events = Event.where(category: @category).sort { |a,b| a.get_full_title.downcase <=> b.get_full_title.downcase }
    else
      @events = Event.where(category: @category).select { |e| e.is_visible? }.sort { |a,b| a.get_full_title.downcase <=> b.get_full_title.downcase }
    end

    @likeusers = @category.likeusers.select { |u| u.is_user? }.sort { |a,b| a.get_sort_name.downcase <=> b.get_sort_name.downcase }
    @likeartists = @category.likeusers.select { |u| u.is_artist? }.sort { |a,b| a.get_sort_name.downcase <=> b.get_sort_name.downcase }
  end

  # GET /categories/new
  def new
    @category = Category.new
    authorize :admin
  end

  # GET /categories/1/edit
  def edit
    authorize :admin
  end

  # POST /categories
  # POST /categories.json
  def create
    @category = Category.new(category_params)
    authorize :admin

    respond_to do |format|
      if @category.save
        format.html { redirect_to @category, notice: I18n.t(:"success.messages.categories.create") }
        format.json { render :show, status: :created, location: @category }
      else
        format.html { render :new }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /categories/1
  # PATCH/PUT /categories/1.json
  def update
    authorize :admin

    respond_to do |format|
      if @category.update(category_params)
        format.html { redirect_to @category, notice: I18n.t(:"success.messages.categories.update") }
        format.json { render :show, status: :ok, location: @category }
      else
        format.html { render :edit }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    authorize :admin

    if !@category.paranoia_destroyed?
      @category.destroy
      respond_to do |format|
        format.html { redirect_to categories_url, notice: I18n.t(:"success.messages.categories.destroy") }
        format.json { head :no_content }
      end
    else
      Category.restore(@category.id, :recursive => true)
      respond_to do |format|
        format.html { redirect_to categories_url, notice: I18n.t(:"success.messages.categories.restore") }
        format.json { head :no_content }
      end
    end
  end

  private

    def set_category
      @category = Category.with_deleted.find(params[:id])
    end

    def category_params
      params.require(:category).permit(:name, :parent_id)
    end

end
