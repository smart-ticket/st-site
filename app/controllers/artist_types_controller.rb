class ArtistTypesController < ApplicationController
  before_action :set_artist_type, only: [:show, :edit, :update, :destroy]

  before_action :authenticate_user!, except: [:index, :show]

  # GET /artist_types
  # GET /artist_types.json
  def index
    if user_signed_in? && current_user.is_admin?
      @artist_types = ArtistType.with_deleted
    else
      @artist_types = ArtistType.all
    end

    @artist_types = @artist_types.sort { |a,b| a.translate_name_completely.downcase <=> b.translate_name_completely.downcase }
  end

  # GET /artist_types/1
  # GET /artist_types/1.json
  def show
    @artists = User.where(artist_type: @artist_type).sort { |a,b| a.get_sort_name.downcase <=> b.get_sort_name.downcase }
  end

  # GET /artist_types/new
  def new
    @artist_type = ArtistType.new
    authorize :admin
  end

  # GET /artist_types/1/edit
  def edit
    authorize :admin
  end

  # POST /artist_types
  # POST /artist_types.json
  def create
    @artist_type = ArtistType.new(artist_type_params)
    authorize :admin

    respond_to do |format|
      if @artist_type.save
        format.html { redirect_to @artist_type, notice: I18n.t(:"success.messages.artist_types.create") }
        format.json { render :show, status: :created, location: @artist_type }
      else
        format.html { render :new }
        format.json { render json: @artist_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /artist_types/1
  # PATCH/PUT /artist_types/1.json
  def update
    authorize :admin

    respond_to do |format|
      if @artist_type.update(artist_type_params)
        format.html { redirect_to @artist_type, notice: I18n.t(:"success.messages.artist_type.update") }
        format.json { render :show, status: :ok, location: @artist_type }
      else
        format.html { render :edit }
        format.json { render json: @artist_type.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    authorize :admin

    if !@artist_type.paranoia_destroyed?
      @artist_type.destroy
      respond_to do |format|
        format.html { redirect_to artist_types_url, notice: I18n.t(:"success.messages.artist_type.destroy") }
        format.json { head :no_content }
      end
    else
      ArtistType.restore(@artist_type.id, recursive: true)
      respond_to do |format|
        format.html { redirect_to artist_types_url, notice: I18n.t(:"success.messages.artist_type.restore") }
        format.json { head :no_content }
      end
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_artist_type
      @artist_type = ArtistType.with_deleted.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def artist_type_params
      params.require(:artist_type).permit(:name)
    end

end
