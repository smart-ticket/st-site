class SocialController < ApplicationController
  before_action :find_user, only: [ :show ]

  before_action :authenticate_user!, except: [:show]

  layout "social"

  def index
    @users = User.users.sort { |a,b| a.get_sort_name.downcase <=> b.get_sort_name.downcase }
    @organizers = User.organizers.select { |u| u.accepted? }.sort { |a,b| a.get_sort_name.downcase <=> b.get_sort_name.downcase }
    @artists = User.artists.sort { |a,b| a.get_sort_name.downcase <=> b.get_sort_name.downcase }
  end

  def show
    if !user_signed_in? && (@user.is_admin? || @user.is_user? || (@user.is_organizer? && !@user.accepted))
      redirect_to root_path, flash: { alert: I18n.t("pundit.not_authorized") }
      return
    elsif (@user != current_user && @user.is_organizer? && !@user.accepted) || @user.is_admin?
      redirect_to social_index_path, flash: { alert: I18n.t("pundit.not_authorized") }
      return
    end

    if @user.is_user?
      @likecategories = @user.likecategories.sort { |a,b| a.translate_name.downcase <=> b.translate_name.downcase }
      @likeorganizers = @user.likeorganizers.sort { |a,b| a.get_name.downcase <=> b.get_name.downcase }
      @likeartists = @user.likeartists.sort { |a,b| a.get_name.downcase <=> b.get_name.downcase }
      @friends = @user.friends.sort { |a,b| a.get_sort_name.downcase <=> b.get_sort_name.downcase }

      if current_user != @user
        @mutual_friends = current_user.friends.select { |u| @friends.include?(u) }.sort { |a,b| a.get_sort_name.downcase <=> b.get_sort_name.downcase }
      end
    elsif @user.is_organizer?
      @fans = @user.fans.sort { |a,b| a.get_sort_name.downcase <=> b.get_sort_name.downcase }
      @events = @user.events.select { |e| e.is_visible? && e.is_base_event? }.sort { |a,b| a.get_title.downcase <=> b.get_title.downcase }
    elsif @user.is_artist?
      @fans = @user.fans.sort { |a,b| a.get_sort_name.downcase <=> b.get_sort_name.downcase }
      @members = @user.members.sort { |a,b| a.get_name.downcase <=> b.get_name.downcase }
      @inverse_members = @user.inverse_members.sort { |a,b| a.get_name.downcase <=> b.get_name.downcase }
      @perform_events = @user.perform_events.select { |e| e.is_visible? && e.is_base_event? }.sort { |a,b| a.get_title.downcase <=> b.get_title.downcase }
    end

    if current_user != @user
      @news_items = @user.news_items.ordered.select { |n| (n.parents == nil || n.parents == []) && n.event == nil && (n.news_item_visibility == :public || @user.friends.include?(current_user) || @user.fans.include?(current_user)) }
    else
      @news_items = @user.news_items.ordered.select { |n| (n.parents == nil || n.parents == []) && n.event == nil }
    end
  end

  def comment_news_item
    news_item = NewsItem.find(params[:news_item][:id])

    comment = NewsItem.create(content: params[:news_item][:news_item][:content], user: current_user)
    comment.save

    commentship = news_item.commentships.build(news_item: news_item, comment: comment)
    commentship.save

    redirect_to :back
  end

  def comment_event
    event = Event.find(params[:event][:id])

    comment = NewsItem.create(content: params[:event][:news_item][:content], user: current_user, event: event)
    comment.save

    redirect_to :back
  end

  def update_comment
    comment = NewsItem.find(params[:news_item][:id])

    if comment.user == current_user
      comment.update_attributes(content: params[:news_item][:content])
    end

    redirect_to :back
  end

  private

    def find_user
      @user = User.all.find(params[:id])
    end

end
