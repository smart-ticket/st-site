class EventsManagerController < ApplicationController
  before_action :find_event, only: [ :edit, :update, :show, :destroy ]

  MAX_GALLERY_IMAGES = 10

  def index
    if current_user == nil || current_user.is_user? || current_user.is_artist?
      user_not_authorized_exception
    else
      @events = filter_base_events(Event.all.ordered)
      @events = filter_current_user_events(@events) if current_user.role != :admin
      @category = params[:category]
      @tag = params[:tag]
      @approved = params[:approved]

      if @category != nil
        @category = Category.with_deleted.find_by_name(@category)
        @events = @events.select { |e| Category.with_deleted.find(e.category_id).is_category?(@category) }
      end

      if @tag != nil
        @events = @events.select { |e| e.has_tag?(@tag) }
      end

      @events_wo_approved = @events

      if @approved != nil
        @approved = @approved
        @events = @events.select { |e| e.approved == @approved }
      end

      @events = @events.sort { |a,b| a.title.downcase <=> b.title.downcase }
      @events_wo_approved = @events_wo_approved.sort { |a,b| a.title.downcase <=> b.title.downcase }

      if current_user.is_organizer? && !current_user.accepted
        flash[:alert] = I18n.t(:"errors.messages.user.not_accepted_organizer_profile_cannot_create_events")
      end

      render "events/" + get_layout + "/index"
    end
  end

  def new
    @event = Event.new
    @event.user = current_user

    authorize @event

    params.permit!
    if params[:parent] != nil
      parent_event = Event.find(params[:parent])
      authorize parent_event

      if parent_event != nil && parent_event.group && parent_event.belongs_to_user(current_user) # skontrolovat ci parentova udalost tiez patri danemu organizatorovi
        @event.parent = parent_event
      end
    end
    if params[:group] == "true"
      @event.group = true
    end

    render "events/" + get_layout + "/new"
  end

  def create
    @event = Event.new(event_params)
    @event.user = current_user

    authorize @event

    respond_to do |format|
      if @event.private
        @event.tag_list = nil
      end

      params.permit!

      if params[:event][:subsubcategory_id] =~ /\A[-+]?[0-9]*\.?[0-9]+\Z/
        @event.category_id = params[:event][:subsubcategory_id]
      elsif params[:event][:subcategory_id] =~ /\A[-+]?[0-9]*\.?[0-9]+\Z/
        @event.category_id = params[:event][:subcategory_id]
      end

      if params[:event][:performships_attributes]
        params[:event][:performships_attributes].values.each do |performship|
          if performship[:_destroy] == "1"
            p = Performship.where(id: performship[:id]).last
            if p != nil
              p.destroy
            end
          elsif performship[:_destroy] == "false"
            p = @event.performships.find { |p| p.user_id.to_s == performship[:user_id] }
            if p == nil
              @event.performships.build(user_id: performship[:user_id])
            end
          end
        end
      end

      if @event.save
        if params[:event][:gallery_images_attributes]
          params[:event][:gallery_images_attributes].values.each do |gallery_image|
            if @event.gallery_images != nil && @event.gallery_images.length >= MAX_GALLERY_IMAGES
              break
            end
            @event.gallery_images.create(gallery_image: gallery_image[:gallery_image], description: gallery_image[:description])
          end
        end

        format.html { redirect_to events_manager_index_path, notice: I18n.t(:"success.messages.events.create") }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render "events/" + get_layout + "/new" }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    authorize @event

    render "events/" + get_layout + "/edit"
  end

  def update
    authorize @event

    respond_to do |format|
      @event.assign_attributes(event_params)

      if @event.private
        @event.tag_list = nil
      end
      if !@event.group && @event.subevents != nil && @event.subevents.size > 0
        @event.subevents.each do |e|
          e.destroy
        end
      end

      params.permit!

      if @event.published
        @event.approved = :waiting_for_approval
      end

      if params[:event][:subsubcategory_id] =~ /\A[-+]?[0-9]*\.?[0-9]+\Z/ # checks if it is a number
        @event.category_id = params[:event][:subsubcategory_id]
      elsif params[:event][:subcategory_id] =~ /\A[-+]?[0-9]*\.?[0-9]+\Z/ # checks if it is a number
        @event.category_id = params[:event][:subcategory_id]
      end

      if params[:event][:gallery_images_attributes]
        params[:event][:gallery_images_attributes].values.each do |gallery_image|
          if @event.gallery_images != nil && @event.gallery_images.length >= MAX_GALLERY_IMAGES
            break
          end
          if gallery_image[:gallery_image] != nil
            @event.gallery_images.create(gallery_image: gallery_image[:gallery_image], description: gallery_image[:description])
          end
        end
      end

      if params[:event][:gallery_images_attributes]
        params[:event][:gallery_images_attributes].values.each do |gallery_image|
          if gallery_image[:id] != nil && gallery_image[:id] != ""
            if gallery_image[:_destroy] == "1"
              img = GalleryImage.find(gallery_image[:id])
              img.destroy if img != nil
            else
              img = GalleryImage.find(gallery_image[:id])
              img.description = gallery_image[:description]
              img.save
            end
          end
        end
      end

      if params[:gallery_images]
        params[:gallery_images].each do |gallery_image|
          if @event.gallery_images != nil && @event.gallery_images.length >= MAX_GALLERY_IMAGES
            break
          end
          @event.gallery_images.create(gallery_image: gallery_image)
        end
      end

      if params[:event][:performships_attributes]
        params[:event][:performships_attributes].values.each do |performship|
          if performship[:_destroy] == "1"
            p = Performship.where(id: performship[:id]).last
            if p != nil
              p.destroy
            end
          elsif performship[:_destroy] == "false"
            p = @event.performships.find { |p| p.user_id.to_s == performship[:user_id] }
            if p == nil
              @event.performships.build(user_id: performship[:user_id])
            end
          end
        end
      end

      if @event.save
        if (params[:category] != nil || params[:tag] != nil || params[:approved] != nil)
          format.html { redirect_to events_manager_filter_path(category: params[:category] != nil ? params[:category] : nil, tag: params[:tag], approved: params[:approved]), notice: notice_text }
        else
          format.html { redirect_to events_manager_index_path, notice: notice_text }
        end

        format.json { render :show, status: :updated, location: @event }
      else
        format.html { render "events/" + get_layout + "/new" }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_featured
    @event = Event.find(params[:events_manager_id])

    authorize @event, :update_featured?

    respond_to do |format|
      if current_user.is_admin?
        if event_params[:featured] != nil
          if event_params[:featured] == "true"
            notice_text = I18n.t(:"success.messages.events.featured")
            @event.featured = true
          else
            notice_text = I18n.t(:"success.messages.events.unfeatured")
            @event.featured = false
          end
        else
          notice_text = I18n.t(:"success.messages.events.update")
        end

        if @event.save
          if (params[:category] != nil || params[:tag] != nil || params[:approved] != nil)
            format.html { redirect_to events_manager_filter_path(category: params[:category] != nil ? params[:category] : nil, tag: params[:tag], approved: params[:approved]), notice: notice_text }
          else
            format.html { redirect_to :back, notice: notice_text }
          end

          format.json { render :show, status: :updated, location: @event }
        else
          format.html { render "events/" + get_layout + "/new" }
          format.json { render json: @event.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  def update_published
    @event = Event.find(params[:events_manager_id])

    authorize @event, :update_published?

    respond_to do |format|
      if event_params[:published] != nil
        if event_params[:published] == "true"
          notice_text = I18n.t(:"success.messages.events.published")
          @event.published = true
          @event.approved = :waiting_for_approval
        else
          notice_text = I18n.t(:"success.messages.events.unpublished")
          @event.published = false
          @event.approved = :editing
        end
      else
        notice_text = I18n.t(:"success.messages.events.update")
      end

      if @event.save
        if (params[:category] != nil || params[:tag] != nil || params[:approved] != nil)
          format.html { redirect_to events_manager_filter_path(category: params[:category] != nil ? params[:category] : nil, tag: params[:tag], approved: params[:approved]), notice: notice_text }
        else
          format.html { redirect_to :back, notice: notice_text }
        end

        format.json { render :show, status: :updated, location: @event }
      else
        format.html { render "events/" + get_layout + "/new" }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_approval_message
    @event = Event.find(params[:events_manager_id])

    authorize @event, :update_approval_message?

    params.permit!

    respond_to do |format|
      if (params[:approve] || params[:not_approve]) && current_user.is_admin?
        approval_message = ApprovalMessage.create(event: @event, content: params[:event][:approval_message_attributes][:content])

        if params[:approve]
          notice_text = I18n.t(:"success.messages.events.approved")
          @event.approved = :is_approved
        else
          notice_text = I18n.t(:"success.messages.events.not_approved")
          @event.approved = :is_not_approved
        end

        if approval_message != nil && approval_message.save && @event.save
          if (params[:category] != nil || params[:tag] != nil || params[:approved] != nil)
            format.html { redirect_to events_manager_filter_path(category: params[:category] != nil ? params[:category] : nil, tag: params[:tag], approved: params[:approved]), notice: notice_text }
          else
            format.html { redirect_to events_manager_index_path, notice: notice_text }
          end

          format.json { render :show, status: :updated, location: @event }
        else
          if (params[:category] != nil || params[:tag] != nil || params[:approved] != nil)
            format.html { redirect_to events_manager_filter_path(category: params[:category] != nil ? params[:category] : nil, tag: params[:tag], approved: params[:approved]), alert: I18n.t(:"errors.messages.event.blank_approval_message") }
          else
            format.html { redirect_to events_manager_index_path, alert: I18n.t(:"errors.messages.event.blank_approval_message") }
          end

          format.json { render json: @event.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  def show
    authorize @event

    if params[:version_id]
      title_image = @event.title_image
      @event = @event.versions.select { |v| v.id.to_s == params[:version_id] && v.item_type == "Event" }.first.reify(has_many: true)
      @event.title_image = title_image
    end

    render "events/" + get_layout + "/show"
  end

  def destroy
    authorize @event

    @event.destroy
    flash[:notice] = notice_text = I18n.t(:"success.messages.events.destroy")
    redirect_to events_manager_filter_path(category: params[:category] != nil ? params[:category] : nil, tag: params[:tag], approved: params[:approved])
  end

  private

    def find_event
      @event = Event.find(params[:id])
    end

    def event_params
      params.require(:event).permit(:user_id, :title_image, :title_image_cache, :title, :subtitle, :category_id, :venue_id, :start_date, :start_time, :end_date, :end_time, :short_description, :description, :categories, :gallery_images, :featured, :published, :private, :video, :artists, :tag_list, :group, :parent_id, :approval_message_attributes)
    end

    def filter_base_events(events)
      events.select { |e| !e.has_parent? }
    end

    def filter_visible_events(events)
      events.select { |e| !e.private && e.published && e.approved == :is_approved }
    end

    def filter_current_user_events(events)
      events.select { |e| e.user == current_user }
    end

end
