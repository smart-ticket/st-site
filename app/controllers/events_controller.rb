class EventsController < ApplicationController
  before_action :find_event, only: [ :show ]

  before_action :authenticate_user!, except: [:index, :show, :events_category]

  layout "application"

  def index
    @events = filter_visible_events(filter_base_events(Event.all.ordered)).select { |e| e.start_date >= Date.today }
    #@category = params[:category]
    #@tag = params[:tag]

    #if @category != nil
    #  @category = Category.find_by_name(@category)
    #  @events = @events.select { |e| e.category.is_category?(@category) }
    #end

    #if @tag != nil
    #  @events = @events.select { |e| e.has_tag?(@tag) }
    #end

    @events = @events.sort { |a,b| a.title.downcase <=> b.title.downcase }
    @events_featured = @events.select { |e| e.featured == true }
    @events_nearest = @events.sort { |a,b| a.start_date <=> b.start_date }
    @events_newest = @events.sort { |a,b| b.id <=> a.id }

    @events_category = Hash.new

    Category.where(parent: nil).each do |c|
      ev_cat = @events.select { |e| e.category.is_category?(c) }
      ev_cat_hash = Hash.new
      ev_cat_hash["all"] = ev_cat
      ev_cat_hash["featured"] = ev_cat.select { |e| e.featured == true }
      ev_cat_hash["nearest"] = ev_cat.sort { |a,b| a.start_date <=> b.start_date }
      ev_cat_hash["newest"] = ev_cat.sort { |a,b| b.id <=> a.id }
      ev_cat_hash["popular"] = ev_cat.sort { |a,b| b.viewed <=> a.viewed }
      ev_cat_hash["subcategories"] = Hash.new
      @events_category[c.name] = ev_cat_hash

      Category.where(parent: c).each do |subc|
        ev_subcat = @events.select { |e| e.category.is_category?(subc) }
        ev_subcat_hash = ev_cat_hash["subcategories"]
        ev_subcat_hash[subc.name] = Hash.new
        ev_subcat_hash[subc.name]["all"] = ev_subcat
        ev_subcat_hash[subc.name]["featured"] = ev_subcat.select { |e| e.featured == true }
        ev_subcat_hash[subc.name]["nearest"] = ev_subcat.sort { |a,b| a.start_date <=> b.start_date }
        ev_subcat_hash[subc.name]["newest"] = ev_subcat.sort { |a,b| b.id <=> a.id }
        ev_subcat_hash[subc.name]["popular"] = ev_subcat.sort { |a,b| b.viewed <=> a.viewed }
      end
      @events_category[c.name] = ev_cat_hash
    end
  end

  def events_category
    @category = Category.with_deleted.find_by_name(params[:category])

    @events = filter_visible_events(filter_base_events(Event.all.ordered)).select { |e| e.start_date >= Date.today }.select { |e| e.category.is_category?(@category) }

    @events_featured = @events.select { |e| e.featured == true }
  end

  def show
    if @event == nil
      flash[:alert] = I18n.t(:"pundit.not_authorized_to_show_event")
      redirect_to root_path
    end
  end

  private

    def find_event
      if user_signed_in? && current_user.is_admin?
        @event = Event.find(params[:id])
      elsif user_signed_in? && current_user.is_organizer?
        @event = Event.find(params[:id])

        if !@event.belongs_to_user(current_user) && !@event.is_visible?
          @event = nil
        end
      else
        @event = filter_visible_events(Event.all.ordered).select { |e| e.id.to_s == params[:id] }
        if @event != nil
          @event = @event.first
        end
      end
    end

    def filter_base_events(events)
      events.select { |e| !e.has_parent? }
    end

    def filter_visible_events(events)
      events.select { |e| e.is_visible? }
    end

end
