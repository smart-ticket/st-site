class VenuesController < ApplicationController
  before_action :set_venue, only: [:show, :edit, :update, :destroy]

  before_action :authenticate_user!, except: [:index, :show]

  # GET /venues
  # GET /venues.json
  def index
    if user_signed_in? && current_user.is_admin?
      @venues = Venue.with_deleted
    else
      @venues = Venue.all
    end

    @venues = @venues.sort { |a,b| a.to_s.downcase <=> b.to_s.downcase }
  end

  # GET /venues/1
  # GET /venues/1.json
  def show
    if user_signed_in? && current_user.is_admin?
      @events = Event.where(venue: @venue).sort { |a,b| a.get_full_title.downcase <=> b.get_full_title.downcase }
    else
      @events = Event.where(venue: @venue).select { |e| e.is_visible? }.sort { |a,b| a.get_full_title.downcase <=> b.get_full_title.downcase }
    end
  end

  # GET /venues/new
  def new
    @venue = Venue.new
    authorize :admin
  end

  # GET /venues/1/edit
  def edit
    authorize :admin
  end

  # POST /venues
  # POST /venues.json
  def create
    @venue = Venue.new(venue_params)
    authorize :admin

    respond_to do |format|
      if @venue.save
        format.html { redirect_to @venue, notice: I18n.t(:"success.messages.venues.create") }
        format.json { render :show, status: :created, location: @venue }
      else
        format.html { render :new }
        format.json { render json: @venue.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /venues/1
  # PATCH/PUT /venues/1.json
  def update
    authorize :admin

    respond_to do |format|
      if @venue.update(venue_params)
        format.html { redirect_to @venue, notice: I18n.t(:"success.messages.venues.update") }
        format.json { render :show, status: :ok, location: @venue }
      else
        format.html { render :edit }
        format.json { render json: @venue.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /venues/1
  # DELETE /venues/1.json
  def destroy
    authorize :admin

    if !@venue.paranoia_destroyed?
      @venue.destroy
      respond_to do |format|
        format.html { redirect_to venues_url, notice: I18n.t(:"success.messages.venues.destroy") }
        format.json { head :no_content }
      end
    else
      Venue.restore(@venue.id, :recursive => true)
      respond_to do |format|
        format.html { redirect_to venues_url, notice: I18n.t(:"success.messages.venues.restore") }
        format.json { head :no_content }
      end
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_venue
      @venue = Venue.with_deleted.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def venue_params
      params.require(:venue).permit(:name, :street, :zip, :city, :lat, :lng, :parent_id)
    end

end
