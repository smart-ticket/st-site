class AdminController < ApplicationController
  def index
    authorize :admin_only
  end
end
