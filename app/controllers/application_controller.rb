class ApplicationController < ActionController::Base
  include Pundit

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_user!

  layout :resolve_layout

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  protected

    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name, :email, :nickname, :role, :password, :password_confirmation, :remember_me) }
      devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:email, :nickname, :password, :remember_me) }
      devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:uid, :name, :surname, :nickname, :date_of_birth, :country_id, :address, :zip, :city, :email, :sex, :profile_facebook, :profile_googleplus, :profile_twitter, :profile_spotify, :avatar, :avatar_cache, :short_description, :description, :web, :artist_type_id, :show_real_name, :password, :password_confirmation, :current_password, memberships_attributes: [:member_id, :_destroy, :id], likecategoryships_attributes: [:category_id, :_destroy, :id]) }
    end

    def resolve_layout
      case action_name
        when "index", "show", "new", "edit"
          get_layout
        else
          "application"
      end
    end

    def get_layout
      if user_signed_in? && current_user.role == :admin
        "admin"
      elsif user_signed_in? && current_user.role == :organizer
        "organizer"
      else
        "application"
      end
    end

  protected

    def user_not_authorized_exception
      flash[:alert] = t "not_authorized", scope: "pundit", default: :default
      redirect_to(request.referrer || root_path)
    end

    # Overwriting the sign_out redirect path method
    def after_sign_out_path_for(resource_or_scope)
      root_path
    end

    def user_not_authorized(exception)
     policy_name = exception.policy.class.to_s.underscore

     if user_signed_in? && !current_user.confirmed?
       flash[:error] = t "email_not_confirmed", scope: "pundit", default: :default
     else
       flash[:error] = t "not_authorized", scope: "pundit", default: :default
     end
     redirect_to(request.referrer || root_path)
    end

end
