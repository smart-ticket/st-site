class EventPolicy < ApplicationPolicy
  attr_reader :user, :event

  def initialize(user, event)
    @user = user
    @event = event
  end

  def index?
    is_authorized
  end

  def show?
    is_authorized
  end

  def new?
    create?
  end

  def edit?
    update?
  end

  def create?
    is_authorized && (!user.pending_reconfirmation? && user.confirmed?)
  end

  def update?
    is_authorized && (!user.pending_reconfirmation? && user.confirmed?)
  end

  def update_featured?
    is_authorized && (!user.pending_reconfirmation? && user.confirmed?)
  end

  def update_published?
    is_authorized && (!user.pending_reconfirmation? && user.confirmed?)
  end

  def update_approval_message?
    is_authorized && (!user.pending_reconfirmation? && user.confirmed?)
  end

  def destroy?
    is_authorized && (!user.pending_reconfirmation? && user.confirmed?)
  end

  private

    def is_authorized
      (user.is_organizer? && event.belongs_to_user(user) && user.accepted) || user.is_admin?
    end

end
