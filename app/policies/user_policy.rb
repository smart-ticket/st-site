class UserPolicy < ApplicationPolicy
  attr_reader :current_user, :user

  def initialize(current_user, user)
    @current_user = current_user
    @user = user
  end

  def index?
    is_authorized
  end

  def show?
    is_authorized
  end

  def new?
    create?
  end

  def edit?
    update?
  end

  def create?
    is_authorized
  end

  def update?
    is_authorized
  end

  def update_accept_message?
    is_authorized
  end

  def destroy?
    is_authorized
  end

  private

    def is_authorized
      current_user.is_admin? && ((user.is_admin? && current_user.persistent) || !user.is_admin?)
    end

end
