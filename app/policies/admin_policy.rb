class AdminPolicy < Struct.new(:user, :admin)

  def index?
    true
  end

  def show?
    true
  end

  def new?
    create?
  end

  def edit?
    update?
  end

  def create?
    is_authorized
  end

  def update?
    is_authorized
  end

  def destroy?
    is_authorized
  end

  private

    def is_authorized
      user.is_admin?
    end

end
