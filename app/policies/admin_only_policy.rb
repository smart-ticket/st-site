class AdminOnlyPolicy < Struct.new(:current_user, :admin_only)

  def index?
    is_authorized
  end

  def show?
    is_authorized
  end

  def new?
    create?
  end

  def edit?
    update?
  end

  def create?
    is_authorized
  end

  def update?
    is_authorized
  end

  def destroy?
    is_authorized
  end

  private

    def is_authorized
      current_user.is_admin?
    end

end
