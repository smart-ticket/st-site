class GalleryImagePolicy < ApplicationPolicy
  attr_reader :user, :event

  def initialize(user, gallery_image)
    @user = user
    @gallery_image = gallery_image
  end

  def index?
    true
  end

  def show?
    true
  end

  def new?
    create?
  end

  def edit?
    update?
  end

  def create?
    user.confirmed?
  end

  def update?
    is_authorized && user.confirmed?
  end

  def destroy?
    is_authorized && user.confirmed?
  end

  private

    def is_authorized
      (user.is_organizer? && gallery_image.event.belongs_to_user(user)) || user.is_admin?
    end

end
