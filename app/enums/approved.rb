class Approved < ClassyEnum::Base
end

class Approved::Editing < Approved
end

class Approved::WaitingForApproval < Approved
end

class Approved::IsNotApproved < Approved
end

class Approved::IsApproved < Approved
end
