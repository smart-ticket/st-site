class Role < ClassyEnum::Base
end

class Role::User < Role
end

class Role::Artist < Role
end

class Role::Organizer < Role
end

class Role::Admin < Role
end
