class NewsItemVisibility < ClassyEnum::Base
end

class NewsItemVisibility::Public < NewsItemVisibility
end

class NewsItemVisibility::Friends < NewsItemVisibility
end

class NewsItemVisibility::Fans < NewsItemVisibility
end
