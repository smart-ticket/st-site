module ActiveRecordExtension

  extend ActiveSupport::Concern

  def valid_attribute?(attribute_name)
    self.valid?
    self.errors[attribute_name].blank?
  end

  module ClassMethods

  end
end

# include the extension
ActiveRecord::Base.send(:include, ActiveRecordExtension)
